import requests
import random

def register_new_user(login, password, email, user_role):
    registerInfo = '{"Login":"' + login + '","HashedPassword":"' + password + '","Email":"' + email + '","UserRole":' + user_role + "}"
    #print(registerInfo)
    headers = {'Content-Type': 'application/json'}
    r = requests.post('http://127.0.0.1:9010/api/core/Users/',data=registerInfo, headers=headers)
    #print("get: " + r.text)
    #print("status_code: " + r.status_code)

    return (r.status_code == 200)

def generate_new_login():
    return 'user_'+ str(random.randint(0, 100000))

def authorize(login, password):
    authorize_info = '{"Login":"' + login + '","HashedPassword":"' + password + '"}'
    print(authorize_info)
    headers = {'Content-Type': 'application/json'}
    r = requests.post('http://127.0.0.1:9010/api/Authorization/',data=authorize_info, headers=headers)
    #print("get: " + r.text)
    
    if (r.status_code != 200):
         return False

    return r.json()

def add_new_character(token, character_string, user_id):
    auth_str = "Bearer " + token
    headers = {'Content-Type': 'application/json', 'Authorization': auth_str}
    r = requests.post('http://127.0.0.1:9010/api/core/Characters/'+user_id ,data=character_string, headers=headers)
    #print("get: " + r.text)
    print("status: " + str(r.status_code))
    
    if (r.status_code != 200):
         return False

    return r.text

def get_characters(token, user_id):
    auth_str = "Bearer " + token
    headers = {'Content-Type': 'application/json', 'Authorization': auth_str}
    r = requests.get('http://127.0.0.1:9010/api/core/Characters/'+user_id , headers=headers)
    #print("get: " + r.text)
    print("status: " + str(r.status_code))
    
    if (r.status_code != 200):
         return False
    return r.json()["List"]

def get_characters_rule_system(token, user_id, rule_system):
    auth_str = "Bearer " + token
    headers = {'Content-Type': 'application/json', 'Authorization': auth_str}
    r = requests.get('http://127.0.0.1:9010/api/core/Characters/?userId=' + user_id + "&ruleSustem=" + rule_system, headers=headers)
    #print("get: " + r.text)
    print("status: " + str(r.status_code))
    
    if (r.status_code != 200):
         return False
    return r.json()["List"]

def get_character(token, user_id, character_id):
    auth_str = "Bearer " + token
    headers = {'Content-Type': 'application/json', 'Authorization': auth_str}
    r = requests.get('http://127.0.0.1:9010/api/core/Characters/'+user_id + '/' + character_id, headers=headers)
    print("get: " + r.text)
    print("status: " + str(r.status_code))
    
    if (r.status_code != 200):
         return False

    return r.json()

def delete_character(token, user_id, character_id):
    auth_str = "Bearer " + token
    headers = {'Content-Type': 'application/json', 'Authorization': auth_str}
    r = requests.delete('http://127.0.0.1:9010/api/core/Characters/'+ user_id + "/" + character_id, headers=headers)
    #print("get: " + r.text)
    print("status: " + str(r.status_code))
    
    if (r.status_code != 200):
         return False

    return r

def put_character(token, user_id, character_id, new_character):
    auth_str = "Bearer " + token
    headers = {'Content-Type': 'application/json', 'Authorization': auth_str}
    r = requests.put('http://127.0.0.1:9010/api/core/Characters/'+ user_id + "/" + character_id, json=new_character, headers=headers)
    #print("get: " + r.text)
    print("status: " + str(r.status_code))
    
    if (r.status_code != 200):
         return False

    return r

def test_updating(testing, tested):
    return (testing == tested)


def test_result(result, error_message):
    if (result == False):
        print("ERROR:"+ error_message)
        return 1


def main():
    login = generate_new_login()
    
    test_result(register_new_user(login, 'pass', "example@gmail.com", "0"), "Error in User Creation")
    
    res = authorize(login, 'pass')
    test_result(res, "Error in Authorize")
    token = res['Token']
    user_info = res['UserInfo']

    user_id = user_info['UserId']
    
    #print(token)
    #print(user_info)
    #print(user_id)

    character1_str = '{"_id":"000000000000000000000000","Name":"Name1","UserId":"' + user_id + '","Gender":0,"Age":20,"Info":"Info","RuleSystem":0,"SystemCharacterData":{"SomeData":"some data"}}'
    character2_str = '{"_id":"000000000000000000000000","Name":"Name2","UserId":"' + user_id + '","Gender":0,"Age":20,"Info":"Info","RuleSystem":1,"SystemCharacterData":{"SomeData":"some data"}}'

    character1_id = add_new_character(token, character1_str, user_id)
    test_result(character1_id, "Error in NewCharacterCreation")
    character1_id = character1_id[16:len(character1_id)-2]
    
    character2_id = add_new_character(token, character2_str, user_id)
    test_result(character2_id, "Error in NewCharacterCreation")
    character2_id = character2_id[16:len(character2_id)-2]
    

    characters = get_characters(token, user_id)
    test_result(characters, "Error in GetCharacters")
    
    #print(character1_id)
    #print(character2_id)
    new_characters = get_characters_rule_system(token, user_id, "0")
    test_result(new_characters, "Error in get_characters_rule_system")
    print("NEW_CHARACTERS")
    print(new_characters)

    character1 = get_character(token, user_id, character1_id)
    test_result(character1, "Error in get_character") 

    character2 = get_character(token, user_id, character2_id)
    test_result(character2, "Error in get_character") 

    #print(character1)
    #print(character2)

    character1['Name'] = "NewName1"
    character2['Name'] = "NewName2"

    res = put_character(token, user_id, character1_id, character1)
    character1_upd = get_character(token, user_id, character1_id)
    test_result(test_updating(character1_upd, character1), "Error in put_character")

    res = put_character(token, user_id, character2_id, character2)
    character2_upd = get_character(token, user_id, character2_id)
    test_result(test_updating(character2_upd, character2), "Error in put_character")

    res = delete_character(token, user_id, character1_id)
    test_result(res, "Error in delete_character")
    
    res = delete_character(token, user_id, character2_id)
    test_result(res, "Error in delete_character")
     

main()
