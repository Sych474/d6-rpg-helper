import requests
 
character1 = '{"_id":"CharacterId1","Name":"Name1","UserId":"UserId1","Gender":0,"Age":20,"Info":"Info","RuleSystem":1,"SystemCharacterData":{"SomeData":"some data"}}'
character2 = '{"_id":"CharacterId2","Name":"Name2","UserId":"UserId2","Gender":0,"Age":20,"Info":"Info","RuleSystem":1,"SystemCharacterData":{"SomeData":"some data"}}'

userInfo = '{"Login":"user","UserId":"000","Email":"email@mail.ru","UserRole":0}'

# CORRECT
def GetCharacterByIdCorrectTest():
    r = requests.get('http://127.0.0.1:9010/api/core/Characters/1/1')
    #print("get: " + r.text)
    result = (r.text == character1) and (r.status_code == 200)
    return result

def GetCharactersByUserIdAndRuleSystemCorrectTest():
    r = requests.get('http://127.0.0.1:9010/api/core/Characters?userId=1&ruleSystem=1')
    #print("get: " + r.text)
    result = (r.text == "["+character1+","+character2+"]") and (r.status_code == 200)
    return result

def GetCharactersByUserIdCorrectTest():
    r = requests.get('http://127.0.0.1:9010/api/core/Characters/1')
    #print("get: " + r.text)
    result = (r.text == "["+character1+","+character2+"]") and (r.status_code == 200)
    return result


# ERRORS
def GetCharacterByIdErrorTest():
    r = requests.get('http://127.0.0.1:9010/api/core/Characters/3/1')
    #print("get: " + r.text)
    result = (r.text == "\"Error\"") and (r.status_code == 500)
    return result

def GetCharactersByUserIdAndRuleSystemErrorTest():
    r = requests.get('http://127.0.0.1:9010/api/core/Characters?userId=3&ruleSystem=1')
    #print("get: " + r.text)
    result = (r.text == "\"Error\"") and (r.status_code == 500)
    return result

def GetCharactersByUserIdErrorTest():
    r = requests.get('http://127.0.0.1:9010/api/core/Characters/3')
    #print("get: " + r.text)
    result = (r.text == "\"Error\"") and (r.status_code == 500)
    return result

# EMPTY
def GetCharactersByUserIdAndRuleSystemEmptyTest():
    r = requests.get('http://127.0.0.1:9010/api/core/Characters?userId=2&ruleSystem=1')
    #print("get: " + r.text)
    result = (r.text == "[]") and (r.status_code == 200)
    return result

def GetCharactersByUserIdEmptyTest():
    r = requests.get('http://127.0.0.1:9010/api/core/Characters/2')
    #print("get: " + r.text)
    result = (r.text == "[]") and (r.status_code == 200)
    return result

# Auth tests
def AuthorizationTest():
    authStr = '{"Login":"user","Password":"HashedPassword":"pass"}'
    r = requests.post('http://127.0.0.1:9010/api/Authorization',json=authStr)
    #TO_DO check token
    print("get: " + r.text)
    result = (r.status_code == 200)
    return result


def DoTest(foo):
    r = foo()
    print(r)
    return r

def main():
    res = True
    res = DoTest(GetCharacterByIdCorrectTest) and res 
    res = DoTest(GetCharacterByIdErrorTest) and res
    res = DoTest(GetCharactersByUserIdAndRuleSystemCorrectTest) and res
    res = DoTest(GetCharactersByUserIdAndRuleSystemErrorTest) and res
    res = DoTest(GetCharactersByUserIdAndRuleSystemEmptyTest) and res
    res = DoTest(GetCharactersByUserIdCorrectTest) and res
    res = DoTest(GetCharactersByUserIdErrorTest) and res
    res = DoTest(GetCharactersByUserIdEmptyTest) and res
    res = DoTest(AuthorizationTest) and res
    
    if (res != True):
        print("WARNING - Tests failed")
        exit(1)
    else:
        print("Success")
        exit(0)

main()