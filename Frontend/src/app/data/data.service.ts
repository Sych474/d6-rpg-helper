import {Injectable, EventEmitter, Inject} from '@angular/core';
import {Character} from '../models/character-short-info';
import {Observable, of} from 'rxjs';
import { HttpService } from '../http/http.service';
import { Api } from '../http/api-urls';
import { UserInfo } from '../models/user-info';
import { map } from 'rxjs/operators';

@Injectable()
export class DataService {
  public event: EventEmitter<any> = new EventEmitter();
  public ELEMENT_DATA: Character[] = [];
  public USERS_DATA: UserInfo[] = [];

  RuleSystems = [
    {shortValue: 'Неизвестная', value: 'Неизвестная', id: 0},
    {shortValue: 'D&D4', value: 'Dungeons and Dragons 4й редакции', id: 1},
    {shortValue: 'СЛИВ', value: 'СЛИВ', id: 2}
  ];
  Genders = [
    {value: 'Женский', id: 0},
    {value: 'Мужской', id: 1}
  ]

  constructor(@Inject(HttpService) public http: any) {
  }

  public getCharactersByUserId() {
    var userInfo: UserInfo = JSON.parse(JSON.parse(localStorage.getItem('userInfo')));
    let userRole = userInfo.UserRole;
    if (userRole == 1) {
      let childUserInfo = localStorage.getItem('childUserInfo');
      if (childUserInfo) {
        userInfo = JSON.parse(childUserInfo);
      }
      else {
        console.log("Can't read childUserInfo in DataService.getCharactersByUserId()");
        this.event.emit(false);
      }
    }

    let url = Api.getCharactersByUserId + userInfo.UserId;
    this.http.getCharactersByUserId(url).subscribe(
      (response: Character[]) => {
        this.ELEMENT_DATA = response["List"];
        this.event.emit(true);
      },
      err => {
        console.log(err);
        this.event.emit(false);
      }
    )
  }

  public getUsers() {
    let url = Api.getUsers;
    this.http.getUsers(url).subscribe(
      (response: Character[]) => {
        this.USERS_DATA = response["List"];
        this.event.emit(true);
      },
      err => {
        console.log(err);
        this.event.emit(false);
      }
    )
  }

  public saveCharacter(characterId: string, character: Character) {
    var userInfo: UserInfo = JSON.parse(JSON.parse(localStorage.getItem('userInfo')));
    let url = Api.saveCharacter + userInfo.UserId + "/" + characterId;
    this.http.saveCharacter(url, character).subscribe(
      () => {
        this.event.emit({
          "success": true,
          "error" : ""
        });
      },
      err => {
        console.log(err);
        this.event.emit({
          "success": false,
          "error" : err
        });
      }
    )
  }

  public deleteCharacter(characterId: string) {
    var userInfo: UserInfo = JSON.parse(JSON.parse(localStorage.getItem('userInfo')));
    let url = Api.deleteCharacter + userInfo.UserId + "/" + characterId;
    this.http.deleteCharacter(url).subscribe(
      () => {
        this.event.emit({
          "success": true,
          "error" : ""
        });
      },
      err => {
        console.log(err);
        this.event.emit({
          "success": false,
          "error" : err
        });
      }
    )
  }

  public addCharacter(character: Character) {
    var userInfo: UserInfo = JSON.parse(JSON.parse(localStorage.getItem('userInfo')));
    let url = Api.addCharacter + userInfo.UserId;
    this.http.addCharacter(url, character).subscribe(
      (characterId) => {
        this.event.emit({
          "success": true,
          "error" : "",
          "data" : characterId["CharacterId"]
        });
      },
      err => {
        console.log(err);
        this.event.emit({
          "success": false,
          "error" : err
        });
      }
    )
  }

  public getData(): Observable<Character[]> {
    return of<Character[]>(this.ELEMENT_DATA);
  }

  public getUsersData(): Observable<UserInfo[]> {
    return of<UserInfo[]>(this.USERS_DATA);
  }

  public getCharacterByCharacterId(id: string): Observable<Character> {
    //return this.getData().pipe(
    //  map((characters: Character[]) => characters.find(character => character._id === id))
    //);
    var userInfo: UserInfo = JSON.parse(JSON.parse(localStorage.getItem('userInfo')));
    let url = Api.getCharacterById + userInfo.UserId + "/" + id;
    return this.http.getCharacterByCharacterId(url);
  }

  public getSystems() {
    return this.RuleSystems;
  }

  public getGenders() {
    return this.Genders;
  }

  public dataLength() {
    return this.ELEMENT_DATA.length;
  }
}