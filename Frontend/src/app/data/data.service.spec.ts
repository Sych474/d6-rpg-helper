import { TestBed } from '@angular/core/testing';

import { DataService } from './data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpService } from '../http/http.service';

import { Character } from '../models/character-short-info';
import { JWToken } from '../models/jwt';
import { Api } from '../http/api-urls';

describe('DataService', () => {
  let service: DataService;
  let http: HttpTestingController;
  let httpservice: HttpService;
  let i: number = 1;
  let jwt: JWToken = {
    Token:"kkrkrofkrogkorgkrlltgr",
    UserInfo:{
      "Login":"0000",
      "UserId":"5be4a2d6f2516c0f94fb0958",
      "Email":"some email",
      "UserRole":0
    }
  };

  beforeEach(() => 
  {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        HttpService,
        DataService
      ]
    });
    http = TestBed.get(HttpTestingController);
    httpservice = TestBed.get(HttpService);
    service = TestBed.get(DataService);
    localStorage.setItem('token', jwt.Token);
    localStorage.setItem('userInfo', JSON.stringify(jwt.UserInfo));
  });

  afterEach(() => {
    localStorage.removeItem("token");
    localStorage.removeItem("userInfo");
    http.verify();
  });

  it('[data-'+(i++).toString()+']should be created', () => {
    expect(service).toBeTruthy();
  });

  it('[data-'+(i++).toString()+'] getCharactersByUserId', () => {
    const url = Api.getCharactersByUserId + jwt.UserInfo.UserId;
    
    service.getCharactersByUserId();

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('GET');
  });

  it('[data-'+(i++).toString()+'] getUsers', () => {
    let url = Api.getUsers;

    service.event.subscribe((data) => {
      expect(data).toEqual(true);
    });

    service.getUsers();

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('GET');
  });

  it('[data-'+(i++).toString()+'] saveCharacter', () => {
    let newCharacter: Character = {
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Changed Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData: {"S": 10, "L": 11, "I": 12, "V": 13}
    }
    let url = Api.saveCharacter + "5be4a2d6f2516c0f94fb0958/" + newCharacter._id;

    service.event.subscribe((data) => {
      //console.log("111"+data);
      expect(data).toEqual(true);
    });

    service.saveCharacter(newCharacter._id, newCharacter);

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('PUT');
  });

  it('[data-'+(i++).toString()+'] deleteCharacter', () => {
    let id = "5bd75aae05d79d3dcc2e6f8e";
    let userId = jwt.UserInfo.UserId;
    let url = Api.deleteCharacter + userId + "/" + id;

    service.event.subscribe((data) => {
      expect(data).toEqual(true);
    });

    service.deleteCharacter(id);

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('DELETE');
  });

  it('[data-'+(i++).toString()+'] addCharacter', () => {
    let newCharacter: Character = {
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Changed Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData: {"S": 10, "L": 11, "I": 12, "V": 13}
    }
    let url = Api.addCharacter + "5be4a2d6f2516c0f94fb0958";

    service.event.subscribe((data) => {
      expect(data).toEqual(true);
    });

    service.addCharacter(newCharacter);

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('POST');
  });

  it('[data-'+(i++).toString()+'] getCharacterByCharacterId', () => {
    let expectedData: Character = {
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData: {"S": 10, "L": 11, "I": 12, "V": 13}
    };

    let id = "5bd6dff40708363dcc5929c3";
    let userId = jwt.UserInfo.UserId;
    let url = Api.getCharacterById + userId + "/" + id;
    service.getCharacterByCharacterId(id).subscribe((data) => {
      expect(data).toEqual(expectedData);
    });

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('GET');
    req.flush(expectedData);
  });

  it('[data-'+(i++).toString()+'][no_token] getCharacterByCharacterId', () => {
    localStorage.removeItem("token");
    
    let id = "5bd75aae05d79d3dcc2e6f8e";
    let userId = jwt.UserInfo.UserId;

    let url = Api.getCharacterById + userId + "/" + id;
    service.event.subscribe(
      (data: boolean) => {
        expect(data).toEqual(false);
      }
    );

    service.getCharacterByCharacterId(id);
    http.expectNone(url);
  });

  it('[data-'+(i++).toString()+'][no_token] getCharactersByUserId', () => {
    localStorage.removeItem("token");

    const url = Api.getCharactersByUserId + jwt.UserInfo.UserId;

    service.event.subscribe(
      (data: boolean) => {
        expect(data).toEqual(false);
      }
    );

    service.getCharactersByUserId();
    http.expectNone(url);
  });

  it('[data-'+(i++).toString()+'][no_token] getUsers', () => {
    localStorage.removeItem("token");

    let url = Api.getUsers;

    service.event.subscribe((data) => {
      expect(data).toEqual(false);
    });

    service.getUsers();
    http.expectNone(url);
  });

  it('[data-'+(i++).toString()+'][no_token] saveCharacter', () => {
    localStorage.removeItem("token");

    let newCharacter: Character = {
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Changed Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData: {"S": 10, "L": 11, "I": 12, "V": 13}
    }
    let url = Api.saveCharacter + "5be4a2d6f2516c0f94fb0958/" + newCharacter._id;

    service.event.subscribe((data) => {
      expect(data.success).toEqual(false);
      expect(data.error).toEqual("Login first!");
    });

    service.saveCharacter(newCharacter._id, newCharacter);
    http.expectNone(url);
  });

  it('[data-'+(i++).toString()+'][no_token] deleteCharacter', () => {
    localStorage.removeItem("token");

    let id = "5bd75aae05d79d3dcc2e6f8e";
    let userId = jwt.UserInfo.UserId;
    let url = Api.deleteCharacter + userId + "/" + id;

    service.event.subscribe((data) => {
      expect(data.success).toEqual(false);
      expect(data.error).toEqual("Login first!");
    });

    service.deleteCharacter(id);
    http.expectNone(url);
  });

  it('[data-'+(i++).toString()+'][no_token] addCharacter', () => {
    localStorage.removeItem("token");

    let newCharacter: Character = {
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Changed Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData: {"S": 10, "L": 11, "I": 12, "V": 13}
    }
    let url = Api.addCharacter + "5be4a2d6f2516c0f94fb0958";

    service.event.subscribe((data) => {
      expect(data.success).toEqual(false);
      expect(data.error).toEqual("Login first!");
    });

    service.addCharacter(newCharacter);
    http.expectNone(url);
  });
});
