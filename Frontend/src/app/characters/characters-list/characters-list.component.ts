import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data/data.service';
import {Character} from '../../models/character-short-info';
import {DataSource} from '@angular/cdk/table';
import {Observable} from 'rxjs/Observable';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-characters-list',
  templateUrl: './characters-list.component.html',
  styleUrls: ['./characters-list.component.css']
})
export class CharactersListComponent implements OnInit {
  displayedColumns: string[] = ['Name', 'Age', 'Gender', 'RuleSystem', 'info', 'edit', 'delete'];
  dataSource = new CharacterDataSource(this.dataService);

  constructor(
    private dataService: DataService,
    private router: Router
    ) {
      localStorage.setItem("newEdit", "false");
  }

  ngOnInit() {
    this.dataService.getCharactersByUserId();
    this.dataService.event.subscribe(
      (result: boolean) => {
        if (result) {
          this.onUpdate();
        }
        else {
          this.router.navigate(['/error']);
        }
      }
    )
  }

  getRuleSystem(id: number): string {
    let systems = this.dataService.getSystems();
    return systems[id].shortValue;
  }

  add() {
    localStorage.setItem("newEdit", "true");
    this.router.navigate(['/list/edit/new']);
  }

  delete(id: string) {
    this.dataService.deleteCharacter(id);
    this.dataService.getCharactersByUserId();
  }

  onUpdate() {
    this.dataSource = new CharacterDataSource(this.dataService);
  }
}

export class CharacterDataSource extends DataSource<any> {
  constructor(private dataService: DataService) {
    super();
  }

  connect(): Observable<Character[]> {
    return this.dataService.getData();
  }

  disconnect() {
  }
}