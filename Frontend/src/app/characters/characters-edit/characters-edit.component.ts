import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { DataService } from "../../data/data.service";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Character } from 'src/app/models/character-short-info';
import { UserInfo } from 'src/app/models/user-info';

@Component({
  selector: 'app-characters-edit',
  templateUrl: './characters-edit.component.html',
  styleUrls: ['./characters-edit.component.css']
})
export class CharactersEditComponent implements OnInit {

  character: Character;
  genders: any;
  ruleSystems: any;
  resultMessage: string = "";
  isNew: boolean = false;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private ngZone: NgZone
    ) {
    this.genders = this.dataService.getGenders();
    this.ruleSystems = this.dataService.getSystems();
  }

  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this.ngZone.onStable.pipe(take(1))
        .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  ngOnInit() {
    this.isNew = localStorage.getItem("newEdit").toLowerCase() == "true";
    var userInfo: UserInfo = JSON.parse(localStorage.getItem('userInfo'));
    let userId = userInfo.UserId;

    if (this.isNew == true) {
      this.character = {
        "Age" : 0,
        "Gender" : 0,
        "Name" : "",
        "RuleSystem" : 0,
        "SystemCharacterData" : {},
        "UserId" : userId,
        "Info" : "",
        "_id" : "000000000000000000000000"
      }
    }
    else {
      this.route.paramMap.pipe(
        switchMap((params: ParamMap) =>
          this.dataService.getCharacterByCharacterId(params.get('id')))
      ).subscribe(
        (char: Character) => {
          this.character = char;
        }
      );
    }
  }

  saveCharacter() {
    localStorage.setItem("newEdit", "false");
    if (this.isNew) {
      this.dataService.addCharacter(this.character);
    }
    else {
      this.dataService.saveCharacter(this.character._id, this.character);
    }
    this.dataService.event.subscribe(
      (success) => {
        if (success["success"] == true) {
          this.dataService.getCharactersByUserId();
          this.router.navigate(["/list"]);
        }
        else {
          this.resultMessage = "Не удалось сохранить. Попробуйте еще раз.";
        }
      },
      err => {
        this.resultMessage = "Не удалось сохранить. Попробуйте еще раз.";
      }
    )
  }
}