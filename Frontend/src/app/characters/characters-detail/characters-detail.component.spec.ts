import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialAppModule } from '../../material.module';
import { AuthService } from '../../auth/auth.service';
import { DataService } from '../../data/data.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CharactersDetailComponent } from './characters-detail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpService } from 'src/app/http/http.service';

describe('CharactersDetailComponent', () => {
  let component: CharactersDetailComponent;
  let fixture: ComponentFixture<CharactersDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialAppModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        AuthService,
        DataService,
        HttpService
      ],
      declarations: [ CharactersDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharactersDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
