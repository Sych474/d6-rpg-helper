import { Component, OnInit } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { DataService } from "../../data/data.service";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Character } from 'src/app/models/character-short-info';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-characters-detail',
  templateUrl: './characters-detail.component.html',
  styleUrls: ['./characters-detail.component.css']
})
export class CharactersDetailComponent implements OnInit {
  character: Character;
  editLink: string;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService
    ) {
      localStorage.setItem("newEdit", "false");
    }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.dataService.getCharacterByCharacterId(params.get('id')))

    ).subscribe(
      (char: Character) => {
        console.log(char);
        this.character = char;
      }
    );

    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.setEditLink(params.get('id')))
    ).subscribe(
      () => {}
    );
  }

  goToEdit() {
    this.router.navigate([this.editLink]);
  }

  setEditLink(id: string): Observable<string> {
    this.editLink = 'list/edit/' + id;
    return of(this.editLink);
  }

  getRuleSystem(id: number): string {
    let systems = this.dataService.getSystems();
    return systems[id].value;
  }

  getGender(id: number): string {
    let genders = this.dataService.getGenders();
    return genders[id].value;
  }
}
