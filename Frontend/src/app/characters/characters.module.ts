import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharactersRoutingModule } from './characters-routing.module';
import { MaterialAppModule } from './../material.module';

import { CharactersListComponent } from './characters-list/characters-list.component';
import { CharactersDetailComponent } from './characters-detail/characters-detail.component';
import { FormsModule } from '@angular/forms';
import { CharactersEditComponent } from './characters-edit/characters-edit.component';

@NgModule({
  declarations: [
    CharactersListComponent,
    CharactersDetailComponent,
    CharactersEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialAppModule,
    CharactersRoutingModule
  ]
})
export class CharactersModule { }
