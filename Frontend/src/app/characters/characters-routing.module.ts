import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CharactersListComponent } from './characters-list/characters-list.component';
import { CharactersDetailComponent } from './characters-detail/characters-detail.component';
import { CharactersEditComponent } from './characters-edit/characters-edit.component';

export const charactersRoutes: Routes = [
  { path: 'list', redirectTo: 'characters-list' },
  { path: 'list/:id', redirectTo: 'characters-list/:id' },
  { path: 'list/edit/:id', redirectTo: 'characters-list/edit/:id' },
  { path: 'characters-list',  component: CharactersListComponent },
  { path: 'characters-list/:id', component: CharactersDetailComponent },
  { path: 'characters-list/edit/:id', component: CharactersEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(charactersRoutes)],
  exports: [RouterModule]
})
export class CharactersRoutingModule { }
