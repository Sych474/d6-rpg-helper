import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import {FormsModule, FormControl, ReactiveFormsModule} from '@angular/forms';

import { DataService } from './data/data.service';
import { AuthService } from './auth/auth.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialAppModule } from './material.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { HttpService } from './http/http.service';
import {HttpClientModule} from '@angular/common/http';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CharactersModule } from './characters/characters.module';
import { RegisterPageComponent } from './auth/register-page/register-page.component';
import { AdminModule } from './admin/admin.module';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    LoginPageComponent,
    PageNotFoundComponent,
    RegisterPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialAppModule,
    HttpClientModule,
    FormsModule,
    CharactersModule,
    AdminModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [
    DataService,
    AuthService,
    HttpService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    LoginPageComponent,
    RegisterPageComponent
  ]
})
export class AppModule { }
