export class RegisterInfo
{
    public loginMin: number = 4;
    public passwordMin: number = 8;
    public validatingMessage: string = "";
    
    constructor(
        public Login: string, 
        public HashedPassword: string,
        public Email: string,
        public UserRole: number = 0 // 0 - user, 1 - admin
        ) { }

    public isValid(): boolean {
        this.validatingMessage = "";
    
        if (this.Login.length < 4) {
            //this.validatingMessage = "Логин должен содержать хотя бы 4 символа.";
            return false;
        }

        if (this.HashedPassword.length < 8) {
            //this.validatingMessage = "Пароль должен содержать хотя бы 8 символов.";
            return false;   
        }
   
        return true;
    }
    public trim(): RegisterInfoShort {
        return new RegisterInfoShort(
            this.Login,
            this.HashedPassword,
            this.Email,
            this.UserRole
            );
    }

}

export class RegisterInfoShort {
    constructor(
        public Login: string, 
        public HashedPassword: string,
        public Email: string,
        public UserRole: number = 0 // 0 - user, 1 - admin
        ) { }
}