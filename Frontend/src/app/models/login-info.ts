export class LoginInfo
{
    public loginMin: number = 4;
    public passwordMin: number = 8;
    public validatingMessage: string = "";

    constructor(public Login: string, public HashedPassword: string) { }

    public isValid(): boolean {
        this.validatingMessage = "";
    
        if (this.Login.length < 4) {
            //this.validatingMessage = "Логин должен содержать хотя бы 4 символа.";
            return false;
        }

        if (this.HashedPassword.length < 8) {
            //this.validatingMessage = "Пароль должен содержать хотя бы 8 символов.";
            return false;   
        }

        /*let passRe: RegExp = new RegExp("/(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}/g");
        console.log(!passRe.test(this.password))
        if (!passRe.test(this.password)) {
            this.validatingMessage = "Пароль должен содержать только числа и символы латинского алфавита, минимум одно число, один заглавный символ и один прописной символ.";
            return false;
        }*/
   
        return true;
    }

    public trim(): LoginInfoShort {
        return new LoginInfoShort(this.Login, this.HashedPassword);
    }
}

export class LoginInfoShort {
    constructor(public Login: string, public HashedPassword: string) { }
}