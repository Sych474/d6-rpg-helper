import { UserInfo } from "./user-info";

export class JWToken
{    
    public Token: string;
    public UserInfo: UserInfo;
}