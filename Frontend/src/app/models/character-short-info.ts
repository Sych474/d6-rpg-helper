export class Character
{
    _id: string;
    Name: string;
    UserId: string;
    Gender: number;
    Age: number;
    Info: string;
    RuleSystem: number;
    SystemCharacterData: any;
}