export class UserInfo
{
    public Login: string;
    public UserId: string;
    public Email: string;
    public UserRole: number;
}