import {Injectable, EventEmitter, Inject} from '@angular/core';
import {Router} from '@angular/router';
import {LoginInfo} from './../models/login-info';
import {HttpService} from './../http/http.service';
import {Api} from './../http/api-urls';
import { JWToken } from './../models/jwt';
import { RegisterInfo } from '../models/register-info';
import { UserInfo } from '../models/user-info';

@Injectable()
export class AuthService {
  public event: EventEmitter<any> = new EventEmitter();

  constructor(
    @Inject(HttpService) public http: any,
    public router: Router) {}

  public login(loginInfo: LoginInfo) {
    let url = Api.auth;

    return this.http.getJToken(url, loginInfo).subscribe(
      (jwt: JWToken) => {
        localStorage.setItem('token', jwt.Token);
        localStorage.setItem('userInfo', JSON.stringify(jwt.UserInfo));
        this.event.emit({
          "success": true,
          "error" : ""
        });
      },
      err => {
        console.log(err);
        this.event.emit({
          "success": false,
          "error" : err
        });
      }
    );
  }

  public register(registerInfo: RegisterInfo) {
    let url = Api.register;

    return this.http.postUser(url, registerInfo).subscribe(
      () => {
        this.event.emit({
          "success": true,
          "error" : ""
        });
      },
      err => {
        console.log(err);
        this.event.emit({
          "success": false,
          "error" : err
        });
      }
    );
  }

  public handleAuthentication(): void {
    if (this.isAuthenticated()) {
      if (this.isAdmin()) {
        this.router.navigate(["/admin"]);
      }
      else {
        this.router.navigate(["/list"]);
      }
    }
    else {
      this.router.navigate(['/']);
    }
  }

  public logout(): void {
    if (this.isAuthenticated())
    {
      localStorage.removeItem('token');
    }
    if (localStorage.getItem('userInfo')) {
      localStorage.removeItem('userInfo');
    }
    this.router.navigate(['/']);
  }

  public isAuthenticated(): boolean {
    //return this.authorized;
    if (localStorage.getItem('token')){
      return true;
    }
    else {
      return false;
    }
  }

  public isAdmin(): boolean {
    if (!this.isAuthenticated()) {
      return false;
    }
    if (localStorage.getItem('userInfo') == null) {
      return false;
    }
    var userInfo: UserInfo = JSON.parse(JSON.parse(localStorage.getItem('userInfo')));
    console.log(userInfo);
    let userRole = userInfo.UserRole;
    if (userRole == 1) {
      return true;
    }
    return false;
  }
}