import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { Observable, of, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { MaterialAppModule } from '../material.module';
import { AppModule } from '../app.module';
import { routes } from '../app-routing.module';

import { HttpService } from '../http/http.service';
import { AuthService } from './auth.service';

import { JWToken } from '../models/jwt';
import { LoginInfo } from '../models/login-info';
import { RegisterInfo } from '../models/register-info';
import { UserInfo } from '../models/user-info';

describe('AuthService', () => {
    let service: AuthService;
    let http: HttpService;

    let router: Router;
    let location: Location;    

    let i: number = 1;
    let jwt: JWToken = {
      Token:"kkrkrofkrogkorgkrlltgr",
      UserInfo:{
        "Login":"0000",
        "UserId":"5be4a2d6f2516c0f94fb0958",
        "Email":"some email",
        "UserRole":0
      }
    };
  
    beforeEach(() => 
    {
        TestBed.configureTestingModule({
            imports: [
            HttpClientTestingModule,
            MaterialAppModule,
            AppModule,
            RouterTestingModule.withRoutes(routes)
            ],
            providers: [
            AuthService,
            {provide: HttpService, useClass: HttpServiceStub}
            ]
        });

        location = TestBed.get(Location);
        router = TestBed.get(Router);
        router.initialNavigation();

        http = TestBed.get(HttpService);
        service = TestBed.get(AuthService);

        localStorage.setItem('token', jwt.Token);
        localStorage.setItem('userInfo', JSON.stringify(jwt.UserInfo));
    });
  
    afterEach(() => {
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");
    });
  
    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('[auth-'+(i++).toString()+'] success login', () => {
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");
        let loginInfo: LoginInfo = new LoginInfo("0000", "12345678");

        // action
        service.event.subscribe(
            (result) => {
                expect(result["success"]).toEqual(true);
                expect(result["error"]).toEqual("");
            }
        );

        service.login(loginInfo);
        
        let token = localStorage.getItem("token");
        let userInfo = JSON.parse(localStorage.getItem("userInfo"));
        
        expect(userInfo).toEqual(jwt.UserInfo);
        expect(token).toEqual(jwt.Token);
    });

    it('[auth-'+(i++).toString()+'] success register', () => {
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");

        let expectedData: UserInfo[] = [
            {
                Login: "1111",
                UserId: "5be4a2d6f2516c0f94fb0958",
                Email: "some email",
                UserRole: 0
            }
        ];

        let registerInfo: RegisterInfo = new RegisterInfo("1111", "12345678", "some email");

        // action
        service.event.subscribe(
            (result) => {
                expect(result["success"]).toEqual(true);
                expect(result["error"]).toEqual("");
            }
        );
        service.register(registerInfo);
        
        let usersAfter;
        http.getUsers("").subscribe(res => {
            usersAfter = res;
        });

        expect(usersAfter).toEqual(expectedData);
    });

    it('[auth-'+(i++).toString()+'][both] not success login', () => {
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");
        let loginInfo: LoginInfo = new LoginInfo("2000", "0000000");

        // action
        service.event.subscribe(
            (result) => {
                expect(result["success"]).toEqual(false);
                expect(result["error"]).toEqual("Unable to login");
            }
        );

        service.login(loginInfo);
        
        let token = localStorage.getItem("token");
        let userInfo = localStorage.getItem("userInfo");
        
        expect(userInfo).toEqual(null);
        expect(token).toEqual(null);
    });

    it('[auth-'+(i++).toString()+'][login] not success login', () => {
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");
        let loginInfo: LoginInfo = new LoginInfo("0001", "12345678");

        // action
        service.event.subscribe(
            (result) => {
                expect(result["success"]).toEqual(false);
                expect(result["error"]).toEqual("Unable to login");
            }
        );

        service.login(loginInfo);
        
        let token = localStorage.getItem("token");
        let userInfo = localStorage.getItem("userInfo");
        
        expect(userInfo).toEqual(null);
        expect(token).toEqual(null);
    });

    it('[auth-'+(i++).toString()+'][password] not success login', () => {
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");
        let loginInfo: LoginInfo = new LoginInfo("0000", "0000000");

        // action
        service.event.subscribe(
            (result) => {
                expect(result["success"]).toEqual(false);
                expect(result["error"]).toEqual("Unable to login");
            }
        );

        service.login(loginInfo);
        
        let token = localStorage.getItem("token");
        let userInfo = localStorage.getItem("userInfo");
        
        expect(userInfo).toEqual(null);
        expect(token).toEqual(null);
    });

    it('[auth-'+(i++).toString()+'] login invalid login', () => {
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");
        let loginInfo: LoginInfo = new LoginInfo("0001", "12345678");

        // action
        service.event.subscribe(
            (result) => {
                expect(result["success"]).toEqual(false);
                expect(result["error"]).toEqual("Unable to login");
            }
        );

        service.login(loginInfo);
        
        let token = localStorage.getItem("token");
        let userInfo = localStorage.getItem("userInfo");
        
        expect(userInfo).toEqual(null);
        expect(token).toEqual(null);
    });

    it('[auth-'+(i++).toString()+'] not success register', () => {
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");

        let registerInfo: RegisterInfo = new RegisterInfo("0000", "12345678", "some email");

        // action
        service.event.subscribe(
            (result) => {
                expect(result["success"]).toEqual(false);
                expect(result["error"]).toEqual("Unable to register");
            }
        );
        service.register(registerInfo);
    });

    it('[auth-'+(i++).toString()+'] isAuthenticated', () => {
        localStorage.removeItem("token");
        let toBeFalse = service.isAuthenticated();

        localStorage.setItem("token", jwt.Token);
        let toBeTrue = service.isAuthenticated();

        expect(toBeFalse).toBeFalsy();
        expect(toBeTrue).toBeTruthy();
    });

    it('[auth-'+(i++).toString()+'] handleAuthentication not logined', () => {
        localStorage.removeItem("token");
        service.handleAuthentication();

        let path = location.path();

        expect(path).toBe('/');
    });

    it('[auth-'+(i++).toString()+'] handleAuthentication was logined as user', fakeAsync(() => {
        localStorage.setItem("token", jwt.Token);
        service.handleAuthentication();
        tick();
        let path = location.path();
        
        expect(path).toBe('/characters-list');
    }));

    it('[auth-'+(i++).toString()+'] handleAuthentication was logined as admin', fakeAsync(() => {
        let adm: UserInfo = {
            "Login":"0000",
            "UserId":"5be4a2d6f2516c0f94fb0958",
            "Email":"some email",
            "UserRole":1
        };
        localStorage.setItem("token", jwt.Token);
        localStorage.setItem("userInfo", JSON.stringify(adm));
        service.handleAuthentication();
        tick();
        let path = location.path();
        
        expect(path).toBe('/admin/panel');
    }));

    it('[auth-'+(i++).toString()+'] isAdmin no token', () => {
        let value;
        localStorage.removeItem("token");
        value = service.isAdmin();
        expect(value).toBeFalsy();
    });

    it('[auth-'+(i++).toString()+'] isAdmin no userInfo', () => {
        let value;
        localStorage.setItem("token", jwt.Token);
        localStorage.removeItem("userInfo");
        value = service.isAdmin();
        expect(value).toBeFalsy();
    });

    it('[auth-'+(i++).toString()+'] isAdmin admin', () => {
        let value;
        let adm: UserInfo = {
            "Login":"0000",
            "UserId":"5be4a2d6f2516c0f94fb0958",
            "Email":"some email",
            "UserRole":1
        };

        localStorage.setItem("token", jwt.Token);
        localStorage.setItem("userInfo", JSON.stringify(adm));
        value = service.isAdmin();
        expect(value).toBeTruthy();
    });

    it('[auth-'+(i++).toString()+'] isAdmin user', () => {
        let value;
        let usr: UserInfo = {
            "Login":"0000",
            "UserId":"5be4a2d6f2516c0f94fb0958",
            "Email":"some email",
            "UserRole":0
        };

        localStorage.setItem("token", jwt.Token);
        localStorage.setItem("userInfo", JSON.stringify(usr));
        value = service.isAdmin();
        expect(value).toBeFalsy();
    });

    it('[auth-'+(i++).toString()+'][was_logined] logout', () => {
        localStorage.setItem("token", jwt.Token);
        localStorage.setItem("userInfo", JSON.stringify(jwt.UserInfo));
        service.logout();

        let value = localStorage.getItem("token");
        expect(value).toBe(null);
        
        value = localStorage.getItem("userInfo");
        expect(value).toBe(null);

        let path = location.path();
        expect(path).toBe('/');
    });

    it('[auth-'+(i++).toString()+'][was_not_logined] logout', () => {
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");
        service.logout();

        let value = localStorage.getItem("token");
        expect(value).toBe(null);
        
        value = localStorage.getItem("userInfo");
        expect(value).toBe(null);

        let path = location.path();
        expect(path).toBe('/');
    });

    it('[auth-'+(i++).toString()+'][was_not_logined_2] logout', () => {
        localStorage.removeItem("token");
        service.logout();

        let value = localStorage.getItem("token");
        expect(value).toBe(null);
        
        value = localStorage.getItem("userInfo");
        expect(value).toBe(null);

        let path = location.path();
        expect(path).toBe('/');
    });
});


class HttpServiceStub {
    users: UserInfo[] = [];

    public getJToken(url: string, loginInfo: LoginInfo): Observable<JWToken> {        
        let jwt: JWToken = {
            Token:"kkrkrofkrogkorgkrlltgr",
            UserInfo:{
              "Login":"0000",
              "UserId":"5be4a2d6f2516c0f94fb0958",
              "Email":"some email",
              "UserRole":0
            }
        };

        if (loginInfo.Login != jwt.UserInfo.Login || loginInfo.HashedPassword != "12345678") {
            return throwError("Unable to login");
        }

        return of(jwt);
    }

    public postUser(url: string, registerInfo: RegisterInfo) {
        let short = registerInfo.trim();

        if (short.Login == "0000") {
            return throwError("Unable to register");
        }

        let us = new UserInfo();
        us = {
            UserId: "5be4a2d6f2516c0f94fb0958",
            UserRole: short.UserRole,
            Login: short.Login,
            Email: short.Email
        };

        this.users.push(us);

        return of({});
    }

    public getUsers(): Observable<UserInfo[]> {
        return of(this.users);
    }
}