import {Component, EventEmitter, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import {LoginInfo} from '../../models/login-info';
import { AuthService } from '../auth.service';
import { RegisterPageComponent } from '../register-page/register-page.component';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})

export class LoginPageComponent {
  hide: boolean = true;
  logined: boolean = false;
  loggingMessage: string = "";

  public static DEFAULT_ERR_MSG: string = "Не удалось войти. Проверьте правильность введенных данных.";
  
  public loginInfo: LoginInfo = new LoginInfo('', '');
  public event: EventEmitter<any> = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<LoginPageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(AuthService) public auth: any,
    public dialog: MatDialog
  ) {
  }

  onNoClick(): void {
    this.event.emit(false);
    this.dialogRef.close();
  }

  onSubmit(): void {
    if (this.loginInfo.isValid()) {
      this.auth.login(this.loginInfo);
      this.auth.event.subscribe(
        (success) => {
          if (success["success"]){
            this.event.emit(true);
            this.dialogRef.close();
          }
          else {
            this.loggingMessage = LoginPageComponent.DEFAULT_ERR_MSG;
            this.logined = false;
          }
        },
        err => {
          this.loggingMessage = LoginPageComponent.DEFAULT_ERR_MSG;
          this.logined = false;
        }
      );
    }
    else {
      this.loggingMessage = LoginPageComponent.DEFAULT_ERR_MSG;
      this.logined = false;
    }
  }

  public openDialog(): void {
    let dialogRef = this.dialog.open(RegisterPageComponent, {
      width: '600px',
      data: ''
    });

    dialogRef.componentInstance.event.subscribe((success: boolean) => {
      if (success) {
        
      }
    },
    err => {});
  }
}