import { LoginPageComponent } from './login-page.component';
import { Component, NgModule } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { MatDialogModule, MatDialog } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { OverlayContainer } from '@angular/cdk/overlay';
import { FormsModule } from '@angular/forms';
import { MaterialAppModule } from 'src/app/material.module';
import { AuthService } from '../auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpService } from 'src/app/http/http.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginPageComponent', () => {
  let dialog: MatDialog;
  let overlayContainerElement: HTMLElement;

  let noop: ComponentFixture<NoopComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ 
        DialogTestModule,
        FormsModule,
        MaterialAppModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        AuthService,
        HttpService,
        { provide: OverlayContainer, useFactory: () => {
          overlayContainerElement = document.createElement('div');
          return { getContainerElement: () => overlayContainerElement };
        }}
      ]
    });

    dialog = TestBed.get(MatDialog);

    noop = TestBed.createComponent(NoopComponent);

  });

  it('shows information without details', () => {
    dialog.open(LoginPageComponent, {
      width: '600px',
      data: ''
    });

    noop.detectChanges(); // Updates the dialog in the overlay

    const h2 = overlayContainerElement.querySelector('#mat-dialog-title-0');

    expect(h2.textContent).toBe('Введите данные для авторизации');
  });
});

// Noop component is only a workaround to trigger change detection
@Component({
  template: ''
})
class NoopComponent {}

const TEST_DIRECTIVES = [
  LoginPageComponent,
  NoopComponent
];

@NgModule({
  imports: [
    MatDialogModule,
    NoopAnimationsModule, 
    MaterialAppModule, 
    FormsModule, 
    HttpClientTestingModule,
    RouterTestingModule
  ],
  exports: TEST_DIRECTIVES,
  declarations: TEST_DIRECTIVES,
  entryComponents: [
    LoginPageComponent
  ],
})
class DialogTestModule { }

