import { Component, EventEmitter, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { AuthService } from '../auth.service';
import { RegisterInfo } from 'src/app/models/register-info';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent {
  hide: boolean = true;
  registered: boolean = false;
  errorMessage: string = "";

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();

  public static DEFAULT_ERR_MSG: string = "Не удалось зарегистрироваться. Попробуйте еще раз.";
  public static USER_ALREADY_EXISTS_ERR_MSG: string = "Такой пользователь уже существует.";
  
  public registerInfo: RegisterInfo = new RegisterInfo('', '', '');
  public event: EventEmitter<any> = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<RegisterPageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(AuthService) public auth: any
  ) {
  }

  onNoClick(): void {
    this.event.emit(false);
    this.dialogRef.close();
  }

  onSubmit(): void {
    if (this.registerInfo.isValid()) {
      this.registerInfo.Email = this.emailFormControl.value;
      this.auth.register(this.registerInfo);
      this.auth.event.subscribe(
        success => {
          if (success["success"]){
            this.event.emit(true);
            this.dialogRef.close();
          }
          else {
            this.errorMessage = RegisterPageComponent.USER_ALREADY_EXISTS_ERR_MSG;
            this.registered = false;
          }
        },
        err => {
          this.errorMessage = RegisterPageComponent.DEFAULT_ERR_MSG;
          this.registered = false;
        }
      );
    }
    else {
      this.errorMessage = RegisterPageComponent.DEFAULT_ERR_MSG;
      this.registered = false;
    }
  }
}