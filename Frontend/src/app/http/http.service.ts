import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { LoginInfo } from '../models/login-info';
import { JWToken } from '../models/jwt';
import { Character } from '../models/character-short-info';
import { RegisterInfo } from '../models/register-info';
import { UserInfo } from '../models/user-info';
  
@Injectable()
export class HttpService{ 
    constructor(private http: HttpClient){ }

    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        console.error('An error occurred:', error.error.message);
      } else {
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
      return throwError(new Error(error.status.toString() + ";" + error.statusText));
    };
    
    public getJToken(url: string, loginInfo: LoginInfo): Observable<JWToken>{
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
      };

      return this.http.post<JWToken>(url, loginInfo.trim(), httpOptions).pipe(
        tap((jwt: JWToken) => jwt),
        catchError(this.handleError)
      );
    }

    public postUser(url: string, registerInfo: RegisterInfo) {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
      };

      return this.http.post(url, registerInfo.trim(), httpOptions).pipe(
        tap(() => {}),
        catchError(this.handleError)
      );
    }

    public getCharactersByUserId(url: string): Observable<Character[]> {
      let token: string = localStorage.getItem('token');
      if (!token){
        return throwError('Login first!');
      }

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer '+ token
        })
      }

      return this.http.get<Character[]>(url, httpOptions).pipe(
        tap((characters:any) => characters["List"]),
        catchError(this.handleError)
      );
    }

    public getCharacterByCharacterId(url: string): Observable<Character> {
      let token: string = localStorage.getItem('token');
      if (!token){
        return throwError('Login first!');
      }

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer '+ token
        })
      }

      return this.http.get<Character>(url, httpOptions).pipe(
        tap((characters:Character) => characters),
        catchError(this.handleError)
      );
    }

    public getUsers(url: string): Observable<UserInfo[]> {
      let token: string = localStorage.getItem('token');
      if (!token){
        return throwError('Login first!');
      }

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer '+ token
        })
      }

      return this.http.get<UserInfo[]>(url, httpOptions).pipe(
        tap((users:any) => users["List"]),
        catchError(this.handleError)
      );
    }

    public saveCharacter(url: string, character: Character) {
      let token: string = localStorage.getItem('token');
      if (!token){
        return throwError('Login first!');
      }

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer '+ token
        })
      }

      return this.http.put(url, character, httpOptions).pipe(
        tap(() => {}),
        catchError(this.handleError)
      );
    }

    public deleteCharacter(url: string) {
      let token: string = localStorage.getItem('token');
      if (!token){
        return throwError('Login first!');
      }

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer '+ token
        })
      }

      return this.http.delete(url, httpOptions).pipe(
        tap(() => {}),
        catchError(this.handleError)
      );
    }

    public addCharacter(url: string, character: Character) {
      let token: string = localStorage.getItem('token');
      if (!token){
        return throwError('Login first!');
      }

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer '+ token
        })
      }

      return this.http.post(url, character, httpOptions).pipe(
        tap((characterId) => characterId),
        catchError(this.handleError)
      );
    }
}