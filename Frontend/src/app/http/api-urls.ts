export class Api
{
    public static domen = "http://localhost:9010/";
    public static auth = Api.domen + "api/Authorization";
    public static getCharactersByUserId = Api.domen + "api/core/Characters/"; // {UserId}
    public static register = Api.domen + "api/core/Users/";
    public static saveCharacter = Api.domen + "api/core/Characters/"; // {UserId}/{CharacterId}
    public static deleteCharacter = Api.domen + "api/core/Characters/"; // {UserId}/{CharacterId}
    public static getCharacterById = Api.domen + "api/core/Characters/"; // {UserId}/{CharacterId}
    public static addCharacter = Api.domen + "api/core/Characters/"; // {UserId}
    public static getUsers = Api.domen + "api/core/Users/";

    constructor() { }
}