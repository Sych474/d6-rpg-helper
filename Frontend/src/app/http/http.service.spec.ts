import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { HttpService } from './http.service';
import { Character } from '../models/character-short-info';
import { LoginInfo } from '../models/login-info';
import { JWToken } from '../models/jwt';
import { RegisterInfo } from '../models/register-info';
import { Api } from './api-urls';

describe('HttpService', () => {
  let http: HttpTestingController;
  let service: HttpService;
  let i:number = 1;

  beforeEach(() => 
  {
    TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      HttpService,
    ]
    });
    
    service = TestBed.get(HttpService);
    http = TestBed.get(HttpTestingController);
    localStorage.setItem("token", "kkrkrofkrogkorgkrlltgr");
  });

  afterEach(() => {
    http.verify();
    localStorage.removeItem("token");
  });

  it('[http-'+(i++).toString()+'] should be created', () => {
    expect(service).toBeTruthy();
  });

  it('[http-'+(i++).toString()+'][valid_url] getCharacterByCharacterId', () => {
    let expectedData: Character = {
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData: {"S": 10, "L": 11, "I": 12, "V": 13}
    };
    
    let url = Api.getCharacterById + "5be4a2d6f2516c0f94fb0958/5bd6dff40708363dcc5929c3";

    service.getCharacterByCharacterId(url).subscribe((data) => {
      expect(data).toEqual(expectedData);
    });

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('GET');
    req.flush(expectedData);
  });

  it('[http-'+(i++).toString()+'][invalid_url_full] getCharacterByCharacterId', () => {
    let url = Api.getCharacterById + "5be4a2d6f2516c0f94fb0958/5bd6dff40708363dcc5929c3";
    let badUrl = "http://localhost:9010/api/Characters/5be4a2d6f2516c0f94fb0958/5bd6dff40708363dcc5929c";
    service.getCharacterByCharacterId(badUrl).subscribe(() => { });
    http.expectOne(badUrl);
    http.expectNone(url);
  });

  it('[http-'+(i++).toString()+'][invalid_url_login] getCharacterByCharacterId', () => {
    let url = Api.getCharacterById + "5be4a2d6f2516c0f94fb0958/5bd6dff40708363dcc5929c3";
    let badUrl = Api.getCharacterById + "blablabla/5bd6dff40708363dcc5929c3";
    service.getCharacterByCharacterId(badUrl).subscribe(() => { });
    http.expectOne(badUrl);
    http.expectNone(url);
  });

  it('[http-'+(i++).toString()+'][invalid_url_character] getCharacterByCharacterId', () => {
    let url = Api.getCharacterById + "5be4a2d6f2516c0f94fb0958/5bd6dff40708363dcc5929c3";
    let badUrl = Api.getCharacterById + "5be4a2d6f2516c0f94fb0958/blablablabla";
    service.getCharacterByCharacterId(badUrl).subscribe(() => { });
    http.expectOne(badUrl);
    http.expectNone(url);
  });

  it('[http-'+(i++).toString()+'][valid_url] getCharactersByUserId', () => {
    let expectedData = [{
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData:{"S": 10, "L": 11, "I": 12, "V": 13}
    },
    {
      _id: "5bd75aae05d79d3dcc2e6f8e",
      Name: "Jon2",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 1,
      Info: "Test character clone",
      RuleSystem: 1,
      SystemCharacterData:{"S": 10, "L": 11, "I": 12, "V": 13}
    }];

    let url = Api.getCharactersByUserId + "5be4a2d6f2516c0f94fb0958";

    service.getCharactersByUserId(url).subscribe((data) => {
      expect(data).toEqual(expectedData);
    });

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('GET');
    req.flush(expectedData);
  });

  it('[http-'+(i++).toString()+'][invalid_url_full] getCharactersByUserId', () => {
    let url = Api.getCharactersByUserId + "5be4a2d6f2516c0f94fb0958";
    let badUrl = "http://localhost:9010/api/Characters/5be4a2d6f2516c0f94fb0958";
    service.getCharacterByCharacterId(badUrl).subscribe(() => { });
    http.expectOne(badUrl);
    http.expectNone(url);
  });

  it('[http-'+(i++).toString()+'][invalid_url_user] getCharactersByUserId', () => {
    let url = Api.getCharactersByUserId + "5be4a2d6f2516c0f94fb0958";
    let badUrl = Api.getCharactersByUserId + "blablalbla";
    service.getCharacterByCharacterId(badUrl).subscribe(() => { });
    http.expectOne(badUrl);
    http.expectNone(url);
  });

  it('[http-'+(i++).toString()+'] getUsers', () => {
    let expectedData = [{
      Login:"0000",
      UserId:"5be4a2d6f2516c0f94fb0958",
      Email:"some email",
      UserRole:1
    }];

    let url = Api.getUsers;

    service.getUsers(url).subscribe((data) => {
      expect(data).toEqual(expectedData);
    });

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('GET');
    req.flush(expectedData);
  });

  it('[http-'+(i++).toString()+'] getJToken', () => {
    let expectedData: JWToken = {
      Token:"kkrkrofkrogkorgkrlltgr",
      UserInfo:{
        "Login":"0000",
        "UserId":"5be4a2d6f2516c0f94fb0958",
        "Email":"some email",
        "UserRole":1
      }
    };

    localStorage.removeItem("token");
    let url = Api.auth;
    let loginInfo: LoginInfo = new LoginInfo("0000", "12345678");
    service.getJToken(url, loginInfo).subscribe((data) => {
      expect(data).toEqual(expectedData);
    });

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('POST');
    req.flush(expectedData);
  });

  it('[http-'+(i++).toString()+'] postUser', () => {
    let expectedData = { };

    localStorage.removeItem("token");
    let registerInfo: RegisterInfo = new RegisterInfo("0000", "12345678", "bla@bla.com");
    let url = Api.register;
    service.postUser(url, registerInfo).subscribe((data) => {
      expect(data).toEqual(expectedData);
    });

    const req = http.expectOne(url);
    expect(req.request.method).toEqual('POST');
    req.flush(expectedData);
  });

  it('[http-'+(i++).toString()+'] saveCharacter', () => {
    let expectedData: Character = {
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData: {"S": 10, "L": 11, "I": 12, "V": 13}
    };

    let newCharacter: Character = {
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Changed Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData: {"S": 10, "L": 11, "I": 12, "V": 13}
    }

    let urlGet = Api.getCharacterById + "5be4a2d6f2516c0f94fb0958/5bd6dff40708363dcc5929c3";
    let url = Api.saveCharacter + "5be4a2d6f2516c0f94fb0958/5bd6dff40708363dcc5929c3";

    service.getCharacterByCharacterId(urlGet).subscribe((data) => {
      expect(data).toEqual(expectedData);
    });

    const req = http.expectOne(urlGet);
    expect(req.request.method).toBe('GET');
    req.flush(expectedData);

    // Saving
    service.saveCharacter(url, newCharacter).subscribe((data) => {
      expect(data).toEqual({});
    });

    const req2 = http.expectOne(url);
    expect(req2.request.method).toBe('PUT');
    req2.flush({});

    // Checking
    service.getCharacterByCharacterId(urlGet).subscribe((data) => {
      expect(data).toEqual(newCharacter);
    });

    const req3 = http.expectOne(urlGet);
    expect(req3.request.method).toBe('GET');
    req3.flush(newCharacter);
  });

  it('[http-'+(i++).toString()+'] deleteCharacter', () => {
    let expectedData = [{
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData:{"S": 10, "L": 11, "I": 12, "V": 13}
    },
    {
      _id: "5bd75aae05d79d3dcc2e6f8e",
      Name: "Jon2",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 1,
      Info: "Test character clone",
      RuleSystem: 1,
      SystemCharacterData:{"S": 10, "L": 11, "I": 12, "V": 13}
    }];

    let expectedDataAfterDelete = [{
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData:{"S": 10, "L": 11, "I": 12, "V": 13}
    }];

    let url = Api.getCharactersByUserId;
    let userId = "5be4a2d6f2516c0f94fb0958";

    // First check
    service.getCharactersByUserId(url + userId).subscribe((data) => {
      expect(data).toEqual(expectedData);
    });

    const req = http.expectOne(url + userId);
    expect(req.request.method).toEqual('GET');
    req.flush(expectedData);

    // DELETE
    let deleteUrl = Api.deleteCharacter + userId + "/" + expectedData[1]._id;

    service.deleteCharacter(deleteUrl).subscribe((data) => {
      expect(data).toEqual({});
    });

    const req2 = http.expectOne(deleteUrl);
    expect(req2.request.method).toEqual('DELETE');
    req2.flush({});

    // Check
    service.getCharactersByUserId(url + userId).subscribe((data) => {
      expect(data).toEqual(expectedDataAfterDelete);
    });

    const req3 = http.expectOne(url + userId);
    expect(req3.request.method).toEqual('GET');
    req3.flush(expectedDataAfterDelete);
  });

  it('[http-'+(i++).toString()+'] addCharacter', () => {
    let newCharacter: Character = {
      _id: "5bd6dff40708363dcc5929c3",
      Name: "Changed Jon",
      UserId: "5be4a2d6f2516c0f94fb0958",
      Gender: 1,
      Age: 20,
      Info: "Шакал",
      RuleSystem: 1,
      SystemCharacterData: {"S": 10, "L": 11, "I": 12, "V": 13}
    }
    
    let expectedData = "5bd6dff40708363dcc5929c3";

    let url = Api.addCharacter;
    let userId = "5be4a2d6f2516c0f94fb0958";

    service.addCharacter(url + userId, newCharacter).subscribe((data) => {
      expect(data).toEqual(expectedData);
    });

    const req = http.expectOne(url + userId);
    expect(req.request.method).toEqual('POST');
    req.flush(expectedData);
  });
});

