import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data/data.service';
import {DataSource} from '@angular/cdk/table';
import {Observable} from 'rxjs/Observable';
import { Router } from '@angular/router';
import { UserInfo } from 'src/app/models/user-info';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {
  displayedColumns: string[] = ['Login', 'Email', 'info'];
  dataSource = new UserDataSource(this.dataService);

  constructor(
    private dataService: DataService,
    private router: Router
    ) {
  }

  ngOnInit() {
    this.dataService.getUsers();
    this.dataService.event.subscribe(
      () => {
        this.onUpdate();
      }
    )
  }

  goToUser(user: UserInfo) {
    localStorage.setItem("childUserInfo", JSON.stringify(user));
    this.router.navigate(['/list']);
  }

  onUpdate() {
    this.dataSource = new UserDataSource(this.dataService);
  }
}

export class UserDataSource extends DataSource<any> {
  constructor(private dataService: DataService) {
    super();
  }

  connect(): Observable<UserInfo[]> {
    return this.dataService.getUsersData();
  }

  disconnect() {
  }
}