import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { FormsModule } from '@angular/forms';
import { MaterialAppModule } from '../material.module';

@NgModule({
  declarations: [AdminPanelComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialAppModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
