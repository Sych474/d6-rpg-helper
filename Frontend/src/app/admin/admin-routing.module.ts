import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';

const adminRoutes: Routes = [
  { path: 'admin', redirectTo: 'admin/panel' },
  { path: 'admin/panel',  component: AdminPanelComponent },
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
