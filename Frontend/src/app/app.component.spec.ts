import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { MaterialAppModule } from './material.module';
import { AuthService } from './auth/auth.service';
import { DataService } from './data/data.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpService } from './http/http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MaterialAppModule,
        HttpClientTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        HttpService,
        AuthService,
        DataService,
        HttpService
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'd6rpgHelper'`, () => {
    localStorage.removeItem("token");
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('d6rpgHelper');
  });

  it('should render mat-sidenav in a mat-sidenav-container tag', () => {
    localStorage.removeItem("token");
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('mat-sidenav-container').textContent)
          .toContain('homeВойтиmenu НРИ Помощник клуба "Шестигранник" homeВойти');
  });
});
