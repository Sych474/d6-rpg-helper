import { Component } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { RouterOutlet, Router } from '@angular/router';
import { slideInAnimation } from './animations';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { MatDialog } from '@angular/material';
import { DataService } from "./data/data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [slideInAnimation]
})

export class AppComponent {
  title = 'd6rpgHelper';
  listMessage = '';

  constructor(
    public auth: AuthService,
    public dataService: DataService,
    public dialog: MatDialog,
    public router: Router
    ) {
    auth.handleAuthentication();
    if (this.auth.isAdmin()) {
      this.listMessage = 'пользователей';
    }
    else {
      this.listMessage = 'персонажей';
    }
  }
  
  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  public openDialog(): void {
    let dialogRef = this.dialog.open(LoginPageComponent, {
      width: '600px',
      data: ''
    });

    dialogRef.componentInstance.event.subscribe((success: boolean) => {
      if (success) {
        if (this.auth.isAdmin()) {
          this.listMessage = 'пользователей';
        }
      }
    },
    err => {});
  }

  public goToCharactersList() {
    if (this.auth.isAdmin()) {
      this.router.navigate(["/admin"]);
    }
    else {
      this.router.navigate(["/list"]);
    }
  }
}