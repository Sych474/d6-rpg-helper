import { browser, by, element, promise, ElementFinder, ElementArrayFinder, protractor } from 'protractor';
import { throwError } from 'rxjs';
import { HomePage } from './home.po';

export class CharactersListPage extends HomePage {
  getTitle(): ElementFinder {
      return element(by.tagName('h3'));
  }

  getTitleText(): promise.Promise<string> {
      return this.getTitle().getText();
  }

  // Headers
  getHeaders(): ElementArrayFinder {
      return element(by.tagName('mat-table')).all(by.css('mat-header-row')).all(by.css('mat-header-cell'));
  }

  getHeadersCount(): promise.Promise<number> {
      return this.getHeaders().count();
  }

  getHeaderById(id: number): promise.Promise<string> {
      return this.getHeaders().get(id).getText();
  }

  // List of characters
  getListOfCharacters(): ElementArrayFinder {
      return element(by.tagName('mat-table')).all(by.css('mat-row'));
  }

  getListOfCharactersCount(): promise.Promise<number> {
      return this.getListOfCharacters().count();
  }

  // First character
  getFirstCharacter(): ElementFinder {
      return this.getListOfCharacters().get(0);
  }

  getFirstCharacterColumnById(id: number): promise.Promise<string> {
      return this.getFirstCharacter().all(by.css('mat-cell')).get(id).getText();
  }

  // Get character by id
  getCharacterById(id: number): ElementArrayFinder {
      return this.getListOfCharacters().get(id).all(by.css('mat-cell'));
  }

  getCharacterColumnById(characterId: number, columnId: number): promise.Promise<string> {
      return this.getCharacterById(characterId).get(columnId).getText();
  }

  // Get more info button
  getMoreInfoButton(): ElementFinder {
      return this.getFirstCharacter().element(by.name('moreInfoIcon'));
  }

  getMoreInfoButtonClicked(): promise.Promise<void> {
    return this.getMoreInfoButton().click();
  }

  getMoreInfoButtonClickedAndWait() {
      browser.wait(this.getMoreInfoButtonClicked());
  }

  getEditButton(): ElementFinder {
    return this.getFirstCharacter().element(by.name('editIcon'));
  }

  getEditButtonClicked(): promise.Promise<void> {
    return this.getEditButton().click();
  }

  getEditButtonClickedAndWait() {
    browser.wait(this.getEditButtonClicked());
  }

  getDeleteButton(): ElementFinder {
    return this.getFirstCharacter().element(by.name('deleteIcon'));
  }

  getDeleteButtonClicked(): promise.Promise<void> {
    return this.getDeleteButton().click();
  }

  getDeleteButtonClickedAndWait() {
    browser.wait(this.getDeleteButtonClicked());
  }

  // Add button
  getAddButton(): ElementFinder {
      return element(by.css(".addButton"));
  }

  getAddButtonText(): promise.Promise<string> {
      return this.getAddButton().getText();
  }

  getAddButtonClicked(): promise.Promise<void> {
      return this.getAddButton().click();
  }

  getAddButtonClickedAndWait() {
      this.getAddButton().click().then(() => {});
  }
}
