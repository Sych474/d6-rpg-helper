import { browser, by, element, promise, ElementFinder, ElementArrayFinder, protractor } from 'protractor';
import { throwError } from 'rxjs';
import { HomePage } from './home.po';

export class LoginPage extends HomePage{
    correctPassword: string;
    correctLogin: string;

    constructor() {
        super();
        this.correctLogin = "0000";
        this.correctPassword = "12345678";
    }

    getListOfLabels() {
        return element(by.css('.navigation-items')).all(by.css('.label'));
    }
    
    getListOfLabelsCount() {
        return this.getListOfLabels().count();
    }

    getListOfLabelsFirstAndLast() {
        return [
            this.getListOfLabels().first().getText(),
            this.getListOfLabels().last().getText()
        ];
    }

    // Login page component
    // Title
    getDialogTitle(): ElementFinder {
        return element(by.tagName('h2'));
    }

    getDialogTitleText(): promise.Promise<string> {
        return this.getDialogTitle().getText();
    }

    // Form
    getDialogFrom(): ElementFinder {
        return element(by.css('.example-form'));
    }

    isDialogFromPresent(): promise.Promise<boolean> {
        return this.getDialogFrom().isPresent();
    }

    // Dialog submit button
    getDialogSubmitButton(): ElementFinder {
        return element(by.css('.submit'));
    }

    getDialogSubmitButtonText(): promise.Promise<string> {
        return this.getDialogSubmitButton().getText();
    }

    isDialogSubmitButtonEnabled(): promise.Promise<boolean> {
        return this.getDialogSubmitButton().isEnabled();
    }

    // Dialog register button
    getDialogRegisterButton(): ElementFinder {
        return element(by.css('.register'));
    }

    getDialogRegisterButtonText(): promise.Promise<string> {
        return this.getDialogRegisterButton().getText();
    }

    getDialogRegisterButtonClicked(): promise.Promise<void> {
        return this.getDialogRegisterButton().click();
    }

    // Dialog close button
    getDialogCloseButton(): ElementFinder {
        return element(by.css('.close'));
    }

    getDialogCloseButtonText(): promise.Promise<string> {
        return this.getDialogCloseButton().getText();
    }

    getDialogCloseButtonClicked(): promise.Promise<void> {
        return this.getDialogCloseButton().click();
    }

    // Forms inputs
    getLoginInput(): ElementFinder {
        return element(by.name('login'));
    }

    getPasswordInput(): ElementFinder {
        return element(by.name('password'));
    }

    setLoginData(loginLength: number, passwordLenght: number) {
        if (loginLength > 10 || passwordLenght > 10) {
            throwError("Too big login or password length!");
        }

        let testLogin = new String(10 ** loginLength);
        let testPassword = new String(10 ** passwordLenght);

        for (let i = 0; i < 10; i++) {
            this.getLoginInput().sendKeys(protractor.Key.BACK_SPACE);
            this.getPasswordInput().sendKeys(protractor.Key.BACK_SPACE);
        }

        this.getLoginInput().sendKeys(testLogin.toString());
        this.getPasswordInput().sendKeys(testPassword.toString());
    }

    login(): promise.Promise<void> {        
        this.getLoginInput().sendKeys(this.correctLogin);
        this.getPasswordInput().sendKeys(this.correctPassword);
        
        return this.getDialogSubmitButton().click();
    }

    loginNotAsAdmin(login: string, password: string): promise.Promise<void> {        
        this.getLoginInput().sendKeys(login);
        this.getPasswordInput().sendKeys(password);
        
        return this.getDialogSubmitButton().click();
    }

    logoutAndWait() {
        let res = this.getLogoutButton().click();
        browser.wait(res);
    }
}
