import { browser, by, element, promise, ElementFinder, ElementArrayFinder, protractor, $$ } from 'protractor';
import { throwError } from 'rxjs';
import { HomePage } from './home.po';

export class CharactersDetailPage extends HomePage {
  getMatTabElements(): ElementArrayFinder {
      return element(by.tagName('mat-tab-body')).all(by.css('.text'));
  }

  getMatTabElementById(id: number): ElementFinder {
      return this.getMatTabElements().get(id);
  }

  getMatTabElementByIdText(id: number): promise.Promise<string> {
      return this.getMatTabElementById(id).getText();
  }

  getCharacterInfo(): ElementFinder {
      return element(by.css('text'));
  }

  getCharacterInfoText(): promise.Promise<string> {
      return this.getCharacterInfo().getText();
  }

  getChangeButton(): ElementFinder {
      return element(by.name('change'));
  }

  getChangeButtonText(): promise.Promise<string> {
      return this.getChangeButton().getText();
  }

  getChangeButtonClicked(): promise.Promise<void> {
    return this.getChangeButton().click();
  }

  getChangeButtonClickedAndWait() {
    browser.wait(this.getChangeButton().click());
  }
}
