import { browser, by, element, promise, ElementFinder, ElementArrayFinder, protractor } from 'protractor';
import { throwError } from 'rxjs';
import { HomePage } from './home.po';

export class AdminPanelPage extends HomePage {
    getTitle(): ElementFinder {
        return element(by.tagName('h3'));
    }

    getTitleText(): promise.Promise<string> {
        return this.getTitle().getText();
    }

    getListOfUsers(): ElementArrayFinder {
        return element(by.tagName('mat-table')).all(by.css('mat-row'));
    }

    getListOfUsersCount(): promise.Promise<number> {
        return this.getListOfUsers().count();
    }

    getFirstUserInfo(): ElementFinder {
        return this.getListOfUsers().get(0).element(by.css("a"));
    }

    getInfoIconClick(): promise.Promise<void> {
        return this.getFirstUserInfo().click();
    }

    getInfoIconClickAndWait() {
        let res = this.getFirstUserInfo().click();
        browser.wait(res);
    }
}
