import { browser, by, element, promise, ElementFinder, ElementArrayFinder, protractor, $$ } from 'protractor';
import { throwError } from 'rxjs';
import { HomePage } from './home.po';

export class CharactersEditPage extends HomePage {
  getAgeInput(): ElementFinder {
      return element(by.name('ageInput'));
      return $$('.mat-form-field-infix>input').get(0);
  }

  getNameInput(): ElementFinder {
    return element(by.name('nameInput'));
    return $$('.mat-form-field-infix>input').get(1);
  }

  getGenderInput(): ElementFinder {
    return element(by.model("character.Gender"));
  }

  getRuleSystemInput(): ElementFinder {
    return element(by.model("character.RuleSystem"));
  }

  getInfoInput(): ElementFinder {
    return element(by.model("character.Info"));
  }

  // Add button
  getSaveButton(): ElementFinder {
      return element(by.name("SaveCharacterButton"));
  }

  isSaveButtonEnabled(): promise.Promise<boolean> {
      return this.getSaveButton().isEnabled();
  }

  getSaveButtonText(): promise.Promise<string> {
      return this.getSaveButton().getText();
  }

  getSaveButtonClicked(): promise.Promise<void> {
      return this.getSaveButton().click();
  }

  getSaveButtonClickedAndWait() {
      let res = this.getSaveButton().click();
      browser.wait(res);
  }
  
  addCharacter(name: string = "TestCharacters"): promise.Promise<void> {
    this.getAgeInput().sendKeys(protractor.Key.BACK_SPACE);
    this.getAgeInput().sendKeys("12");
    for (let i = 0; i < 14; i++) {
        this.getNameInput().sendKeys(protractor.Key.BACK_SPACE);
    }
    return this.getNameInput().sendKeys(name);
  }
}
