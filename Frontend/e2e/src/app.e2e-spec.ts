import { AppPage } from './app.po';
import { LoginPage } from './login-page.po';
import { RegisterPage } from './register-page.po';
import { browser } from 'protractor';
import { HomePage } from './home.po';
import { CharactersListPage } from './characters-list.po';
import { AdminPanelPage } from './admin-panel.po';
import { CharactersEditPage } from './characters-edit.po';
import { CharactersDetailPage } from './characters-detail.po';

describe('workspace-project App', () => {
  let page: AppPage;
  let loginPage: LoginPage;
  let registerPage: RegisterPage;
  let homePage: HomePage;
  let charactersList: CharactersListPage;
  let charactersDetail: CharactersDetailPage;
  let charactersEdit: CharactersEditPage;
  let adminPanel: AdminPanelPage;

  beforeEach(() => {
    page = new AppPage();
    loginPage = new LoginPage();
    registerPage = new RegisterPage();
    homePage = new HomePage();
    charactersList = new CharactersListPage();
    charactersEdit = new CharactersEditPage();
    charactersDetail = new CharactersDetailPage();
    adminPanel = new AdminPanelPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();

    let expectedText: string = 'Добро пожаловать в гости к Помощнику КНИ "Шестигранник"!\n';
    expectedText += 'Помощник создан, чтобы облегчить труд ролевиков по созданию и генерации листов персонажей.\n';
    expectedText += 'Нажмите "Войти", чтобы присоединиться к нам!';
    expect(page.getParagraphText()).toEqual(expectedText);
  });

  it('should open login-page dialog', () => {
    homePage.navigateToHome();

    // Button should exist
    expect(homePage.isLoginButtonPresent()).toBeTruthy("Login button must exist!");
    expect(homePage.getLoginButtonText()).toBe("Войти");

    homePage.getLoginButtonClicked().then(
      () => {
        // Login dialog should open
        // Checking title
        expect(loginPage.getDialogTitleText()).toBe("Введите данные для авторизации");
        // Checking form  
        expect(loginPage.isDialogFromPresent()).toBeTruthy("Login form should exist!");
        // Checking submit button is disabled
        expect(loginPage.isDialogSubmitButtonEnabled()).toBeFalsy("Button Войти should be disabled!");
        
        // Checking buttons text
        expect(loginPage.getDialogRegisterButtonText()).toBe("Зарегистрироваться");
        expect(loginPage.getDialogCloseButtonText()).toBe("Отмена");
        expect(loginPage.getDialogSubmitButtonText()).toBe("Войти");
      }
    );
  });

  it('should open register-page dialog', () => {
    homePage.navigateToHome();

    // Button should exist
    expect(homePage.isLoginButtonPresent()).toBeTruthy("Login button must exist!");
    expect(homePage.getLoginButtonText()).toBe("Войти");

    homePage.getLoginButtonClickedAndWait();

    loginPage.getDialogRegisterButtonClicked().then(
      () => {
        // Login dialog should open
        // Checking title
        expect(registerPage.getDialogTitleText()).toBe("Введите данные для регистрации");
        // Checking form  
        expect(registerPage.isDialogFromPresent()).toBeTruthy("Login form should exist!");
        // Checking submit button is disabled
        expect(registerPage.isDialogSubmitButtonEnabled()).toBeFalsy("Button Зарегистрироваться should be disabled!");
        
        // Checking buttons text
        expect(registerPage.getDialogCloseButtonText()).toBe("Отмена");
        expect(registerPage.getDialogSubmitButtonText()).toBe("Зарегистрироваться");
      }
    )
  });

  it('should disable/enable login-page submit button', () => {
    homePage.navigateToHome();

    homePage.getLoginButtonClickedAndWait();

    loginPage.setLoginData(2, 2);
    expect(loginPage.isDialogSubmitButtonEnabled()).toBeFalsy("(2;2) Should be disabled!");

    loginPage.setLoginData(2, 8);
    expect(loginPage.isDialogSubmitButtonEnabled()).toBeFalsy("(2;8) Should be disabled!");

    loginPage.setLoginData(4, 2);
    expect(loginPage.isDialogSubmitButtonEnabled()).toBeFalsy("(4;2) Should be disabled!");

    loginPage.setLoginData(4, 8);
    expect(loginPage.isDialogSubmitButtonEnabled()).toBeTruthy("(4;8) Should be enabled!");

    loginPage.getDialogCloseButtonClicked().then(() => {});
  });

  it('should login as admin', () => {
    homePage.navigateToHome();

    homePage.getLoginButtonClickedAndWait();
    // Checking title
    expect(loginPage.getDialogTitleText()).toBe("Введите данные для авторизации");
    // Checking form  
    expect(loginPage.isDialogFromPresent()).toBeTruthy("Login form should exist!");

    loginPage.login().then(
      () => {
        expect(loginPage.getListOfLabelsCount()).toEqual(2);
        let listOfLabels = loginPage.getListOfLabelsFirstAndLast();
        expect(listOfLabels[0]).toEqual("Список пользователей");
        expect(listOfLabels[1]).toEqual("Выйти");
      }
    );
  });

  it('should show admin panel', () => {
    homePage.navigateToAdminPanel();
    
    expect(adminPanel.getTitleText()).toBe("Все пользователи");
    expect(adminPanel.getListOfUsersCount()).toBe(1);
  });

  it('should show characters list of user [admin]', () => {
    homePage.navigateToAdminPanel();
    adminPanel.getInfoIconClickAndWait();

    expect(charactersList.getTitleText()).toBe('Ваши персонажи');
    expect(charactersList.getHeadersCount()).toBe(7);
    expect(charactersList.getHeaderById(0)).toBe('Имя');
    expect(charactersList.getHeaderById(1)).toBe('Возраст');
    expect(charactersList.getHeaderById(2)).toBe('Пол');
    expect(charactersList.getHeaderById(3)).toBe('Система');
    expect(charactersList.getHeaderById(4)).toBe('');
    expect(charactersList.getHeaderById(5)).toBe('');
    expect(charactersList.getHeaderById(6)).toBe('');

    //expect(charactersList.getListOfCharactersCount()).toBe(2);
    expect(charactersList.getFirstCharacterColumnById(0)).toBe('Jon');
    expect(charactersList.getFirstCharacterColumnById(1)).toBe('20');
    expect(charactersList.getFirstCharacterColumnById(2)).toBe('М');
    expect(charactersList.getFirstCharacterColumnById(3)).toBe('D&D4');

    expect(charactersList.getAddButton().getText()).toBe('Добавить персонажа');
  });

  it('should add character [admin]', () => {
    homePage.navigateToAdminPanel();
    adminPanel.getInfoIconClickAndWait();

    expect(charactersList.getAddButtonText()).toBe('Добавить персонажа');
    charactersList.getAddButtonClicked().then(
      () => {
        expect(charactersEdit.isSaveButtonEnabled()).toBeFalsy('Save button should be disabled!');
        charactersEdit.addCharacter("TestCharacters").then(
          () => {
            //expect(charactersEdit.getNameInput().getText()).toBe('TestCharacters');
            expect(charactersEdit.isSaveButtonEnabled()).toBeTruthy('Save button should be enabled!');
            charactersEdit.getSaveButtonClicked().then(
              () => {
                //expect(charactersList.getListOfCharactersCount()).toBe(3);
                expect(charactersList.getCharacterColumnById(2, 0)).toBe('TestCharacters');
                expect(charactersList.getCharacterColumnById(2, 1)).toBe('12');
                expect(charactersList.getCharacterColumnById(2, 2)).toBe('Ж');
                expect(charactersList.getCharacterColumnById(2, 3)).toBe('Неизвестная');
              }
            );  
          }
        );
      }
    );
  });

  it('should show character detail [admin]', () => {
    browser.wait(homePage.navigateToAdminPanel());
    adminPanel.getInfoIconClickAndWait();
    charactersList.getMoreInfoButtonClicked().then(
      () => {
        browser.sleep(3000);
        expect(charactersDetail.getMatTabElementByIdText(0)).toBe("Имя: Jon");
        expect(charactersDetail.getMatTabElementByIdText(1)).toBe("Возраст: 20");
        expect(charactersDetail.getMatTabElementByIdText(2)).toBe("Пол: Мужской");
        expect(charactersDetail.getMatTabElementByIdText(3)).toBe("Система правил: Dungeons and Dragons 4й редакции");
      }
    );
  });

  it('should edit character from detail [admin]', () => {
    browser.wait(homePage.navigateToAdminPanel());
    adminPanel.getInfoIconClickAndWait();
    charactersList.getMoreInfoButtonClicked().then(
      () => {
        browser.wait(browser.sleep(3000));
        expect(charactersDetail.getMatTabElementByIdText(0)).toBe("Имя: Jon");
        expect(charactersDetail.getMatTabElementByIdText(1)).toBe("Возраст: 20");
        expect(charactersDetail.getMatTabElementByIdText(2)).toBe("Пол: Мужской");
        expect(charactersDetail.getMatTabElementByIdText(3)).toBe("Система правил: Dungeons and Dragons 4й редакции");

        expect(charactersDetail.getChangeButtonText()).toBe('Изменить');

        charactersDetail.getChangeButtonClicked().then(
          () => {
            browser.wait(browser.sleep(3000));
            expect(charactersEdit.getNameInput().getText()).toBe('');
            expect(charactersEdit.isSaveButtonEnabled()).toBeTruthy('Save button should be enabled!');
            charactersEdit.getSaveButtonClicked().then(
              () => {
                expect(charactersList.getCharacterColumnById(2, 0)).toBe('TestCharacters');
                expect(charactersList.getCharacterColumnById(2, 1)).toBe('12');
                expect(charactersList.getCharacterColumnById(2, 2)).toBe('Ж');
                expect(charactersList.getFirstCharacterColumnById(3)).toBe('D&D4');
              }
            );
          }
        )
      }
    );
  });

  it('should edit character [admin]', () => {
    browser.wait(homePage.navigateToAdminPanel());
    adminPanel.getInfoIconClickAndWait();
    charactersList.getEditButtonClicked().then(
      () => {
        browser.sleep(3000);
        charactersEdit.addCharacter().then(
          () => {
            expect(charactersEdit.getNameInput().getText()).toBe('');
            expect(charactersEdit.isSaveButtonEnabled()).toBeTruthy('Save button should be enabled!');
            charactersEdit.getSaveButtonClicked().then(
              () => {
                expect(charactersList.getCharacterColumnById(2, 0)).toBe('TestCharacters');
                expect(charactersList.getCharacterColumnById(2, 1)).toBe('12');
                expect(charactersList.getCharacterColumnById(2, 2)).toBe('Ж');
                expect(charactersList.getCharacterColumnById(2, 3)).toBe('Неизвестная');
              }
            );  
          }
        );
      }
    );
  });

  it('should logout from admin', () => {
    homePage.navigateToHome();
    loginPage.logoutAndWait();
    expect(homePage.getLoginButtonText()).toBe("Войти");
  });

  it('should disable/enable register-page submit button', () => {
    homePage.navigateToHome();
    homePage.getLoginButtonClickedAndWait();

    loginPage.getDialogRegisterButtonClicked().then(
      () => {
        registerPage.setRegisterData(2, 2);
        expect(registerPage.isDialogSubmitButtonEnabled()).toBeFalsy("(2;2) Should be disabled!");

        registerPage.setRegisterData(2, 8);
        expect(registerPage.isDialogSubmitButtonEnabled()).toBeFalsy("(2;8) Should be disabled!");

        registerPage.setRegisterData(4, 2);
        expect(registerPage.isDialogSubmitButtonEnabled()).toBeFalsy("(4;2) Should be disabled!");

        registerPage.setRegisterData(4, 8);
        expect(registerPage.isDialogSubmitButtonEnabled()).toBeTruthy("(4;8) Should be enabled!");
      }
    );
  });

  it('should not register user', () => {
    homePage.navigateToHome();
    homePage.getLoginButtonClickedAndWait();
    
    loginPage.getDialogRegisterButtonClicked().then(
      () => {
        registerPage.registerInvalid().then(
          () => {
            expect(registerPage.getErrorMessageText()).toBe('Такой пользователь уже существует.');
          }
        );
      }
    );
  });

  it('should register user', () => {
    homePage.navigateToHome();
    homePage.getLoginButtonClickedAndWait();
    
    loginPage.getDialogRegisterButtonClicked().then(
      () => {
        registerPage.register().then(
          () => {
            expect(registerPage.getErrorMessageText()).toBe('');
          }
        );
      }
    );
  });

  it('should login as user', () => {
    homePage.navigateToHome();

    homePage.getLoginButtonClickedAndWait();
    loginPage.loginNotAsAdmin(registerPage.newLogin, registerPage.newPassword).then(
      () => {
        expect(loginPage.getListOfLabelsCount()).toEqual(2);
        let listOfLabels = loginPage.getListOfLabelsFirstAndLast();
        expect(listOfLabels[0]).toEqual("Список персонажей");
        expect(listOfLabels[1]).toEqual("Выйти");
      }
    );
  });

  it('should show characters list of user [user]', () => {
    browser.wait(homePage.navigateToCharactersList());

    expect(charactersList.getTitleText()).toBe('Ваши персонажи');
    expect(charactersList.getHeadersCount()).toBe(7);
    expect(charactersList.getHeaderById(0)).toBe('Имя');
    expect(charactersList.getHeaderById(1)).toBe('Возраст');
    expect(charactersList.getHeaderById(2)).toBe('Пол');
    expect(charactersList.getHeaderById(3)).toBe('Система');
    expect(charactersList.getHeaderById(4)).toBe('');
    expect(charactersList.getHeaderById(5)).toBe('');
    expect(charactersList.getHeaderById(6)).toBe('');

    expect(charactersList.getListOfCharactersCount()).toBe(0);

    expect(charactersList.getAddButton().getText()).toBe('Добавить персонажа');
  });

  it('should add character [user]', () => {
    browser.wait(homePage.navigateToCharactersList());

    expect(charactersList.getAddButtonText()).toBe('Добавить персонажа');
    charactersList.getAddButtonClicked().then(
      () => {
        browser.wait(browser.sleep(3000));
        expect(charactersEdit.isSaveButtonEnabled()).toBeFalsy('Save button should be disabled!');
        charactersEdit.addCharacter("Jon").then(
          () => {
            //expect(charactersEdit.getNameInput().getText()).toBe('TestCharacters');
            expect(charactersEdit.isSaveButtonEnabled()).toBeTruthy('Save button should be enabled!');
            charactersEdit.getSaveButtonClicked().then(
              () => {
                //expect(charactersList.getListOfCharactersCount()).toBe(3);
                expect(charactersList.getCharacterColumnById(0, 0)).toBe('Jon');
                expect(charactersList.getCharacterColumnById(0, 1)).toBe('12');
                expect(charactersList.getCharacterColumnById(0, 2)).toBe('Ж');
                expect(charactersList.getCharacterColumnById(0, 3)).toBe('Неизвестная');
              }
            );  
          }
        );
      }
    );
  });

  it('should show character detail [user]', () => {
    browser.wait(homePage.navigateToCharactersList());
    charactersList.getMoreInfoButtonClicked().then(
      () => {
        browser.sleep(3000);
        expect(charactersDetail.getMatTabElementByIdText(0)).toBe("Имя: Jon");
        expect(charactersDetail.getMatTabElementByIdText(1)).toBe("Возраст: 12");
        expect(charactersDetail.getMatTabElementByIdText(2)).toBe("Пол: Женский");
        expect(charactersDetail.getMatTabElementByIdText(3)).toBe("Система правил: Неизвестная");
      }
    );
  });

  it('should edit character from detail [user]', () => {
    browser.wait(homePage.navigateToCharactersList());
    charactersList.getMoreInfoButtonClicked().then(
      () => {
        browser.wait(browser.sleep(3000));
        expect(charactersDetail.getMatTabElementByIdText(0)).toBe("Имя: Jon");
        expect(charactersDetail.getMatTabElementByIdText(1)).toBe("Возраст: 12");
        expect(charactersDetail.getMatTabElementByIdText(2)).toBe("Пол: Женский");
        expect(charactersDetail.getMatTabElementByIdText(3)).toBe("Система правил: Неизвестная");

        expect(charactersDetail.getChangeButtonText()).toBe('Изменить');

        charactersDetail.getChangeButtonClicked().then(
          () => {
            browser.wait(charactersEdit.addCharacter("TestCharacters"));
            expect(charactersEdit.getNameInput().getText()).toBe('');
            expect(charactersEdit.isSaveButtonEnabled()).toBeTruthy('Save button should be enabled!');
            charactersEdit.getSaveButtonClicked().then(
              () => {
                expect(charactersList.getCharacterColumnById(0, 0)).toBe('Jon');
                expect(charactersList.getCharacterColumnById(0, 1)).toBe('112');
                expect(charactersList.getCharacterColumnById(0, 2)).toBe('Ж');
                expect(charactersList.getCharacterColumnById(0, 3)).toBe('Неизвестная');
              }
            );
          }
        )
      }
    );
  });

  it('should edit character [user]', () => {
    browser.wait(homePage.navigateToCharactersList());
    charactersList.getEditButtonClicked().then(
      () => {
        browser.sleep(3000);
        charactersEdit.addCharacter().then(
          () => {
            expect(charactersEdit.getNameInput().getText()).toBe('');
            expect(charactersEdit.isSaveButtonEnabled()).toBeTruthy('Save button should be enabled!');
            charactersEdit.getSaveButtonClicked().then(
              () => {
                expect(charactersList.getCharacterColumnById(0, 0)).toBe('TestCharacters');
                expect(charactersList.getCharacterColumnById(0, 1)).toBe('1112');
                expect(charactersList.getCharacterColumnById(0, 2)).toBe('Ж');
                expect(charactersList.getCharacterColumnById(0, 3)).toBe('Неизвестная');
              }
            );  
          }
        );
      }
    );
  });

  it('should logout from user', () => {
    homePage.navigateToHome();
    loginPage.logoutAndWait();
    expect(homePage.getLoginButtonText()).toBe("Войти");
  });
});
