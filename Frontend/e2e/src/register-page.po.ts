import { browser, by, element, promise, ElementFinder, ElementArrayFinder, protractor } from 'protractor';
import { throwError } from 'rxjs';
import { HomePage } from './home.po';

export class RegisterPage extends HomePage {
    newPassword: string;
    newLogin: string;

    constructor() {
        super();
        this.newLogin = "0001";
        this.newPassword = "12345678";
    }
  // Register page component
  // Title
    getDialogTitle(): ElementFinder {
        return element(by.tagName('h3'));
    }

    getDialogTitleText(): promise.Promise<string> {
        return this.getDialogTitle().getText();
    }

    // Form
    getDialogFrom(): ElementFinder {
        return element(by.css('.example-form'));
    }

    isDialogFromPresent(): promise.Promise<boolean> {
        return this.getDialogFrom().isPresent();
    }

    // Dialog submit button
    getDialogSubmitButton(): ElementFinder {
        return element(by.cssContainingText('.submit', 'Зарегистрироваться'));
    }

    getDialogSubmitButtonText(): promise.Promise<string> {
        return this.getDialogSubmitButton().getText();
    }

    isDialogSubmitButtonEnabled(): promise.Promise<boolean> {
        return this.getDialogSubmitButton().isEnabled();
    }

    // Dialog close button
    getDialogCloseButton(): ElementFinder {
        return element(by.css('.rclose'));
    }

    getDialogCloseButtonText(): promise.Promise<string> {
        return this.getDialogCloseButton().getText();
    }
  
  // Forms inputs
    getLoginInput(): ElementFinder {
        return element(by.name('rlogin'));
    }

    getPasswordInput(): ElementFinder {
        return element(by.name('rpassword'));
    }

    getEmailInput(): ElementFinder {
        return element(by.name('email'));
    }

    setRegisterData(loginLength: number, passwordLenght: number) {
        if (loginLength > 10 || passwordLenght > 10) {
            throwError("Too big login or password length!");
        }

        let testLogin = new String(10 ** loginLength);
        let testPassword = new String(10 ** passwordLenght);

        for (let i = 0; i < 10; i++) {
            this.getLoginInput().sendKeys(protractor.Key.BACK_SPACE);
            this.getPasswordInput().sendKeys(protractor.Key.BACK_SPACE);
            this.getEmailInput().sendKeys(protractor.Key.BACK_SPACE);
        }

        this.getLoginInput().sendKeys(testLogin.toString());
        this.getPasswordInput().sendKeys(testPassword.toString());
        this.getEmailInput().sendKeys("email@ima.com");
    }

    register() {
        this.getLoginInput().sendKeys(this.newLogin);
        this.getPasswordInput().sendKeys(this.newPassword);
        this.getEmailInput().sendKeys("email@ima.com");
        
        return this.getDialogSubmitButton().click();
    }

    getErrorMessage(): ElementFinder {
        return element(by.css('.errorMessage'));
    }

    getErrorMessageText(): promise.Promise<string> {
        return this.getErrorMessage().getText();
    }

    registerInvalid() {
        this.getLoginInput().sendKeys("0000");
        this.getPasswordInput().sendKeys(this.newPassword);
        this.getEmailInput().sendKeys("email@ima.com");
        
        return this.getDialogSubmitButton().click();
    }
}
