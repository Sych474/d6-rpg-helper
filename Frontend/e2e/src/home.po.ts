import { browser, by, element, promise, ElementFinder, ElementArrayFinder } from 'protractor';

export class HomePage {
    navigateToHome(): promise.Promise<any> {
        return browser.get('/');
    }

    navigateToAdminPanel(): promise.Promise<any> {
        return browser.get('/admin-panel');
    }

    navigateToCharactersList(): promise.Promise<any> {
        return browser.get('/characters-list');
    }

    // Login button
    getLoginButton(): ElementFinder {
        return element(by.cssContainingText('.navigation-items .label', 'Войти'));
    }

    // Logout button
    getLogoutButton(): ElementFinder {
        //return element(by.cssContainingText('.navigation-items .label', 'Выйти'));
        return element(by.css('.navigation-items')).all(by.css('.label')).last();
    }

    isLoginButtonPresent(): promise.Promise<boolean> {
        return this.getLoginButton().isPresent();
    }

    getLoginButtonText(): promise.Promise<string> {
        return this.getLoginButton().getText();
    }

    getLoginButtonClicked(): promise.Promise<void> {
        return this.getLoginButton().click();
    }

    getLoginButtonClickedAndWait() {
        let res = this.getLoginButton().click();
        browser.wait(res);
    }
}