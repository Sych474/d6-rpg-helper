import bottle
from bottle import static_file, route, Bottle
from bottle import response
from FakeServer import names
from FakeServer import cors

app = application = bottle.default_app()

@app.route('/')
def root():
    response.headers['Content-Type'] = 'text/html; charset=utf-8'
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'

    return static_file("hack.txt", root = 'C:/Users/yura1/Desktop/bmstu/7 sem/web/lab1/static/html')

@app.route('/hack')
def hack():
    return static_file("index.html", root = 'C:/Users/yura1/Desktop/bmstu/7 sem/web/lab1/static/html')

if __name__ == '__main__':
    app.run(host = '127.0.0.1', port = 9010)
