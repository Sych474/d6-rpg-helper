from bottle import request, response
from bottle import post, get, put, delete
import random
import re, json

def getRandomId():
    res = ""
    for i in range(24):
        res += str(random.choice(range(9)))
        
    return res

class User():
    def __init__(self, login, password, characters=[]):
        self.login = login
        self.password = password
        self.characters = characters
        self.id = getRandomId()
        self.role = 0
        


def findUserById(users, ind):
    for i in range(len(users)):
        if users[i].id == ind:
            return users[i]
    return None

def findCharacterById(arr, ind):
    for i in range(len(arr)):
        if arr[i]["_id"] == ind:
            return i
    return None

users = []
users.append(User("0000","12345678"))
users[0].id = "5be4a2d6f2516c0f94fb0958"
users[0].role = 1
users[0].characters = [
  {
    "_id": "5bd6dff40708363dcc5929c3",
    "Name": "Jon",
    "UserId": "5be4a2d6f2516c0f94fb0958",
    "Gender": 1,
    "Age": 20,
    "Info": "Шакал",
    "RuleSystem": 1,
    "SystemCharacterData":{"S": 10, "L": 11, "I": 12, "V": 13}
  },
  {
    "_id": "5bd75aae05d79d3dcc2e6f8e",
    "Name": "Jon2",
    "UserId": "5be4a2d6f2516c0f94fb0958",
    "Gender": 1,
    "Age": 1,
    "Info": "Test character clone",
    "RuleSystem": 1,
    "SystemCharacterData":{"S": 10, "L": 11, "I": 12, "V": 13}
  }
]

# Adding new character
@post('/api/core/Characters/<userId>')
def add_character(userId):
    global users
    try:
        if not request.headers['Authorization'] == 'Bearer kkrkrofkrogkorgkrlltgr':
            raise ValueError
        
    except ValueError:
        response.status = 404
    try:
        # parse input data
        try:
            data = request.json
        except:
            raise ValueError

        if data is None:
            raise ValueError

    except ValueError:
        # if bad request data, return 400 Bad Request
        response.status = 400
        return
    
    except KeyError:
        # if name already exists, return 409 Conflict
        response.status = 409
        return

    except AttributeError:
        response.status = 404
        return

    user = findUserById(users, userId)

    newId = getRandomId()
    user.characters.append({
                "_id": newId,
                "Name": data["Name"],
                "UserId": userId,
                "Gender": data['Gender'],
                "Age": data['Age'],
                "Info": data['Info'],
                "RuleSystem": data['RuleSystem'],
                "SystemCharacterData":data['SystemCharacterData']
            })
    
    # return 200 Success
    response.headers['Content-Type'] = 'application/json'

    return json.dumps({"CharacterId" : newId})

# Register user
@post('/api/core/Users/')
def register_user():
    global users
    try:
        # parse input data
        try:
            data = request.json
        except:
            raise ValueError

        if data is None:
            raise ValueError

        # extract and validate name
        try:
            login = data['Login']
            password = data['HashedPassword']
            email = data['Email']

            for i in users:
               if login == i.login:
                    raise ValueError
            
        except (TypeError, KeyError):
            raise ValueError

    except ValueError:
        # if bad request data, return 400 Bad Request
        response.status = 400
        return
    
    except KeyError:
        # if name already exists, return 409 Conflict
        response.status = 409
        return

    except AttributeError:
        response.status = 404
        return

    users.append(User(login, password))
    
    # return 200 Success
    response.headers['Content-Type'] = 'application/json'

    return json.dumps({})



# Authorization
@post('/api/Authorization')
def creation_handler():
    global users
    index = 0
    try:
        # parse input data
        try:
            data = request.json
        except:
            raise ValueError

        if data is None:
            raise ValueError

        # extract and validate name
        try:
            flag = True
            login = data['Login']
            password = data['HashedPassword']
            
            for i in range(len(users)):
                if users[i].login == login:
                    if users[i].password == password:
                        index = i
                        flag = False

            if flag:
                raise AttributeError
            
        except (TypeError, KeyError):
            raise ValueError

    except ValueError:
        # if bad request data, return 400 Bad Request
        response.status = 400
        return
    
    except KeyError:
        # if name already exists, return 409 Conflict
        response.status = 409
        return

    except AttributeError:
        response.status = 404
        return

    # add user
    jtoken= 'kkrkrofkrogkorgkrlltgr'
    userInfo='{"Login":"'+str(login)
    userInfo+='","UserId":"'+users[index].id+'","Email":"bla@bla.com","UserRole":'
    userInfo+=str(users[index].role)+'}'
    
    # return 200 Success
    response.headers['Content-Type'] = 'application/json'

    return {'Token': jtoken, 'UserInfo': userInfo}
                 

# get list of characters by userId
@get('/api/core/Characters/<userId>')
def listing_handler(userId):
    global users
    try:
        if not request.headers['Authorization'] == 'Bearer kkrkrofkrogkorgkrlltgr':
            raise ValueError
        
    except ValueError:
        response.status = 404
    
    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'

    user = findUserById(users, userId)

    return json.dumps({'List': user.characters})
                 

# get all users
@get('/api/core/Users/')
def listing_handler():
    global users
    '''Handles name listing'''
    try:
        if not request.headers['Authorization'] == 'Bearer kkrkrofkrogkorgkrlltgr':
            raise ValueError
        
    except ValueError:
        response.status = 404
    
    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'
    
    return json.dumps({'List': [{"Login":users[i].login,
                                 "UserId":users[i].id,
                                 "Email":"some email",
                                 "UserRole":users[i].role} for i in range(len(users))]})


# Changing character by userId and characterId
@put('/api/core/Characters/<userId>/<characterId>')
def update_handler(userId, characterId):
    global users
    try:
        if not request.headers['Authorization'] == 'Bearer kkrkrofkrogkorgkrlltgr':
            raise ValueError
        
    except ValueError:
        response.status = 404
    try:
        # parse input data
        try:
            #data = json.load(utf8reader(request.body))
            data = request.json
        except:
            raise ValueError

        # extract and validate new name
        try:
            user = findUserById(users, userId)
            character = findCharacterById(user.characters, characterId)
            user.characters[character] = {
                "_id": user.characters[character]["_id"],
                "Name": data["Name"],
                "UserId": data['UserId'],
                "Gender": data['Gender'],
                "Age": data['Age'],
                "Info": data['Info'],
                "RuleSystem": data['RuleSystem'],
                "SystemCharacterData": data['SystemCharacterData']
            }
        except (TypeError, KeyError):
            raise ValueError

    except ValueError:
        response.status = 400
        return
    except KeyError as e:
        response.status = e.args[0]
        return

    # return 200 Success
    response.headers['Content-Type'] = 'application/json'

    return json.dumps({})


@get('/api/core/Characters/<userId>/<characterId>')
def update_handler(userId, characterId):
    global users
    try:
        if not request.headers['Authorization'] == 'Bearer kkrkrofkrogkorgkrlltgr':
            raise ValueError
        
    except ValueError:
        response.status = 404
    try:
        # parse input data
        try:
            #data = json.load(utf8reader(request.body))
            data = request.json
        except:
            raise ValueError

        # extract and validate new name
        try:
            user = findUserById(users, userId)
            character = findCharacterById(user.characters, characterId)
            
        except (TypeError, KeyError):
            raise ValueError

    except ValueError:
        response.status = 400
        return
    except KeyError as e:
        response.status = e.args[0]
        return

    # return 200 Success
    response.headers['Content-Type'] = 'application/json'

    return json.dumps(user.characters[character])



# Deleting character
@delete('/api/core/Characters/<userId>/<characterId>')
def delete_handler(userId, characterId):
    global characters
    '''Handles name updates'''
    try:
        if not request.headers['Authorization'] == 'Bearer kkrkrofkrogkorgkrlltgr':
            raise ValueError
        
    except ValueError:
        response.status = 404

    response.status = 200
    # Remove name
    index = 0
    user = findUserById(users, userId)
    for i in range(len(user.characters)):
        if user.characters["_id"] == characterId:
            index = i
            break
    user.characters = user.characters[:index] + user.characters[index+1:]         
    
    return json.dumps({})

