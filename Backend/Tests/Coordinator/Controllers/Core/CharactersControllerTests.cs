﻿using Coordinator.ApiClients;
using Coordinator.ApiClients.Core;
using Coordinator.Authorization;
using Coordinator.Authorization.Exceptions;
using Coordinator.Controllers.Core;
using Model.Database;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Utilits.JsonArray;
using Utilits.String;
using Xunit;

namespace Tests.Coordinator.Controllers.Core
{
    public class CharactersControllerTests
    {
        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public async void TestGetAllWithRuleSystemCorrect()
        {
            //Init
            Mock<ICoreCharacterApiClient> characterApiClient = new Mock<ICoreCharacterApiClient>();
            characterApiClient.Setup(c => c.GetCharacters("userId", RuleSystem.Unknown))
                .ReturnsAsync(new List<JObject>()
                    {
                        JObject.Parse("{\"value\":1}"),
                        JObject.Parse("{\"value\":2}")
                    });

            CharactersController controller = new CharactersController(
                characterApiClient.Object,
                new JsonArrayBuilder(),
                new StringCutter());

            //Action
            ContentResult responce = (ContentResult) await controller.Get("userId", RuleSystem.Unknown);

            //Assert
            Assert.Equal("{\"List\":[{\"value\":1},{\"value\":2}]}", responce.Content);
            Assert.Equal(200, responce.StatusCode);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public async void TestGetAllWithRuleSystemExeptionInApiClient()
        {
            //Init
            Mock<ICoreCharacterApiClient> characterApiClient = new Mock<ICoreCharacterApiClient>();
            characterApiClient.Setup(c => c.GetCharacters("userId", RuleSystem.Unknown))
                .Throws(new Exception("Some exceprion"));
            
            CharactersController controller = new CharactersController(
                characterApiClient.Object,
                new JsonArrayBuilder(),
                new StringCutter());

            //Action
            ContentResult responce = (ContentResult)await controller.Get("userId", RuleSystem.Unknown);

            //Assert
            Assert.Equal("\"Some exceprion\"", responce.Content);
            Assert.Equal(500, responce.StatusCode);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        //[Fact]
        //public async void TestGetAllWithRuleSystemIllegalName()
        //{
        //    //Init
        //    Mock<ICoreCharacterApiClient> characterApiClient = new Mock<ICoreCharacterApiClient>();

        //    CharactersController controller = new CharactersController(
        //        characterApiClient.Object,
        //        new JsonArrayBuilder(),
        //        new StringCutter());

        //    //Action
        //    ContentResult responce = (ContentResult)await controller.Get(null, RuleSystem.Unknown);

        //    //Assert
        //    Assert.Equal("\"Message\"", responce.Content);
        //    Assert.Equal(404, responce.StatusCode);
        //}

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public async void TestGetAllWithRuleSystemIllegalRuleSystem()
        {
            //Init
            Mock<ICoreCharacterApiClient> characterApiClient = new Mock<ICoreCharacterApiClient>();

            CharactersController controller = new CharactersController(
                characterApiClient.Object,
                new JsonArrayBuilder(),
                new StringCutter());

            //Action
            ContentResult responce = (ContentResult)await controller.Get("Some", (RuleSystem) (-1));

            //Assert
            Assert.Equal("\"Unsapported RuleSystem\"", responce.Content);
            Assert.Equal(501, responce.StatusCode);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        //[Fact]
        //public async void TestGetAllWithRuleSystemRightsException()
        //{
        //    //Init
        //    Mock<ICoreCharacterApiClient> characterApiClient = new Mock<ICoreCharacterApiClient>();

        //    CharactersController controller = new CharactersController(
        //        characterApiClient.Object,
        //        new JsonArrayBuilder(),
        //        new StringCutter());

        //    //Action
        //    ContentResult responce = (ContentResult)await controller.Get("Some", (RuleSystem)(-1));

        //    //Assert
        //    Assert.Equal("\"Unresolved operation\"", responce.Content);
        //    Assert.Equal(403, responce.StatusCode);
        //}

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public async void TestGetAllWithRuleSystemJsonReaderException()
        {
            //Init
            Mock<ICoreCharacterApiClient> characterApiClient = new Mock<ICoreCharacterApiClient>();
            characterApiClient
                .Setup(c => c.GetCharacters(It.IsAny<string>(), It.IsAny<RuleSystem>()))
                .Throws(new Newtonsoft.Json.JsonReaderException());

            CharactersController controller = new CharactersController(
                characterApiClient.Object,
                new JsonArrayBuilder(),
                new StringCutter());

            //Action
            ContentResult responce = (ContentResult)await controller.Get("UserId", RuleSystem.Unknown);

            //Assert
            Assert.Equal(500, responce.StatusCode);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public async void TestGetAllWithRuleSystemHttpRequestException()
        {
            //Init
            Mock<ICoreCharacterApiClient> characterApiClient = new Mock<ICoreCharacterApiClient>();
            characterApiClient
                .Setup(c => c.GetCharacters(It.IsAny<string>(), It.IsAny<RuleSystem>()))
                .Throws(new HttpRequestException("Message"));

            CharactersController controller = new CharactersController(
                characterApiClient.Object,
                new JsonArrayBuilder(),
                new StringCutter());

            //Action
            ContentResult responce = (ContentResult)await controller.Get("UserId", RuleSystem.Unknown);

            //Assert
            Assert.Equal("\"Message\"", responce.Content);
            Assert.Equal(504, responce.StatusCode);
        }
    }
}
