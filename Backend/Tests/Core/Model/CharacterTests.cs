﻿using Model.Database;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests.Core.Model
{
    public class CharacterTests
    {
        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestToJsonString()
        {
            //Init
            Character character = TestCharacterCreator.GetTestCharacter();

            //Action
            string json = character.ToJson();

            //Assert
            Assert.Equal(
                "{\"_id\":\"000000000000000000000000\",\"Name\":\"Name\",\"UserId\":\"UserId\",\"Gender\":0,\"Age\":18,\"Info\":\"Info\",\"RuleSystem\":0,\"SystemCharacterData\":{ \"field\" : 0 }}",
                json);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestToJsonConvert()
        {
            //Init
            Character character = TestCharacterCreator.GetTestCharacter();

            //Action + Assert
            // if json is invalid - JObjectParse will throw error!
            JObject json = JObject.Parse(character.ToJson());
        }
    }
}
