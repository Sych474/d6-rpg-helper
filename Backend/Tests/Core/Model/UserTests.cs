﻿using Model.Database;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests.Core.Model
{
    public class UserTests
    {
        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestToJsonString()
        {
            //Init
            User user = new User();
            user._id = new MongoDB.Bson.ObjectId();
            user.Login = "example";
            user.HashedPassword = "password";
            user.Email = "example@gmail.com";
            user.UserRole = UserRole.User;

            //Action

            var json = JsonConvert.SerializeObject(user);
            
            //Assert
            Assert.Equal(
                "{\"_id\":\"000000000000000000000000\",\"Login\":\"example\",\"HashedPassword\":\"password\",\"Email\":\"example@gmail.com\",\"UserRole\":0}",
                json);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestToJsonConvert()
        {
            //Init
            User user = new User();
            user._id = new MongoDB.Bson.ObjectId();
            user.Login = "example";
            user.HashedPassword = "password";
            user.Email = "example@gmail.com";
            user.UserRole = UserRole.User;

            //Action + Assert
            // if json is invalid - JObjectParse will throw error!
            JObject json = JObject.Parse(JsonConvert.SerializeObject(user));
        }
    }
}
