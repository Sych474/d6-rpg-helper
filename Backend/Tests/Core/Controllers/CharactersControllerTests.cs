﻿using Core.Controllers;
using Core.Database.DataProviders;
using Model.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using Moq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Utilits.JsonArray;
using Xunit;

namespace Tests.Core.Controllers
{
    public class CharactersControllerTests
    {
        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestGetAllForCorrectUser()
        {
            //Init
            Mock<Character> character = new Mock<Character>();

            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            List<Character> characters = new List<Character> { character.Object, character.Object };
            characterProvider.Setup(p => p.GetCharacters(It.IsAny<string>())).Returns(characters);

            Mock<IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();
            jsonArrayBuilder.Setup(b => b.BuildArray(characters)).Returns("[{},{}]");


            CharactersController controller = new CharactersController(
                characterProvider.Object, 
                jsonArrayBuilder.Object
                );

            //Action
            ContentResult responce = (ContentResult) controller.Get("userId");
            
            //Assert
            Assert.Equal("[{},{}]", responce.Content);
            Assert.Equal(200, responce.StatusCode);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestGetAllForNullUser()
        {
            //Init
            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            Mock<IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();
            
            CharactersController controller = new CharactersController(
                characterProvider.Object,
                jsonArrayBuilder.Object
                );

            //Action
            ContentResult responce = (ContentResult)controller.Get(null);

            //Assert
            Assert.Equal("\"Illegal UserId\"", responce.Content);
            Assert.Equal(500, responce.StatusCode);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestGetAllForInternalError()
        {
            //Init
            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            characterProvider.Setup(c => c.GetCharacters(It.IsAny<string>())).Throws(new Exception("test"));
            Mock<IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();

            CharactersController controller = new CharactersController(
                characterProvider.Object,
                jsonArrayBuilder.Object
                );

            //Action
            ContentResult responce = (ContentResult)controller.Get("someId");

            //Assert
            Assert.Equal("\"test\"", responce.Content);
            Assert.Equal(500, responce.StatusCode);
        }

        /// <summary>
        /// author - Yury Sokolov
        /// </summary>
        [Fact]
        public void TestGetOneForCorrectUser()
        {
            //Init
            Mock<Character> character = new Mock<Character>();

            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            
            MongoDB.Bson.ObjectId id = new MongoDB.Bson.ObjectId();
            characterProvider.Setup(p => p.GetCharacter("userId", id)).Returns(character.Object);

            Mock<IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();
            
            CharactersController controller = new CharactersController(
                characterProvider.Object,
                jsonArrayBuilder.Object
                );
            //Action
            ContentResult response = (ContentResult)controller.GetOne("userId", id.ToString());

            //Assert
            Assert.Equal(character.Object.ToJson(), response.Content);
            Assert.Equal(200, response.StatusCode);
        }

        /// <summary>
        /// author - Yury Sokolov
        /// </summary>
        [Fact]
        public void TestGetOneForInternalError()
        {
            //Init
            Mock<Character> character = new Mock<Character>();
            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            
            characterProvider.Setup(p => p.GetCharacter(It.IsAny<string>(),
                It.IsAny<MongoDB.Bson.ObjectId>())).Throws(new Exception("test"));

            Mock<IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();

            CharactersController controller = new CharactersController(
                characterProvider.Object,
                jsonArrayBuilder.Object
                );

            MongoDB.Bson.ObjectId id = new MongoDB.Bson.ObjectId();

            //Action
            ContentResult response = (ContentResult)controller.GetOne("userId", id.ToString());

            //Assert
            Assert.Equal("\"test\"", response.Content);
            Assert.Equal(500, response.StatusCode);
        }

        /// <summary>
        /// author - Yury Sokolov
        /// </summary>
        [Fact]
        public void TestPostForEmptyBody()
        {
            Mock<Character> character = new Mock<Character>();

            // Init
            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            characterProvider.Setup(c => c.AddNewCharacter(It.IsAny<Character>()));

            Mock <IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();
            
            CharactersController controller = new CharactersController(
                characterProvider.Object,
                jsonArrayBuilder.Object
                );

            Mock<JObject> jObject = new Mock<JObject>(character.Object.ToJson());
            
            ContentResult response = (ContentResult)controller.Post(jObject.Object,"someUserId");
            ArgumentNullException expectedExeption = new ArgumentNullException("json");

            Assert.Equal($"\"{expectedExeption.Message}\"", response.Content);
            Assert.Equal(500, response.StatusCode);
        }

        /// <summary>
        /// author - Yury Sokolov
        /// </summary>
        [Fact]
        public void TestPostForCorrectCharacter()
        {
            // Init
            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            characterProvider.Setup(c => c.AddNewCharacter(It.IsAny<Character>()));

            Mock<IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();

            CharactersController controller = new CharactersController(
                characterProvider.Object,
                jsonArrayBuilder.Object
                );

            string characterJson = @"{
                '_id': '5bd6dff40708363dcc5929c3',
                'Name': 'Character name',
                'UserId': '5bd6dff40708363dcc5929c4',
                'Gender': 0,
                'Age': 20,
                'Info': 'some text information about character, like history', 
                'RuleSystem': 0,
                'SystemCharacterData': {

                        }
                }";

            JObject jObject = JObject.Parse(characterJson);

            ContentResult response = (ContentResult)controller.Post(jObject, "someUserId");

            characterProvider.Verify(c => c.AddNewCharacter(It.IsAny<Character>()), Times.Once());

            Assert.Equal($"\"{jObject.Value<string>("_id")}\"", response.Content);
            Assert.Equal(200, response.StatusCode);
        }

        /// <summary>
        /// author - Yury Sokolov
        /// </summary>
        [Fact]
        public void TestPutForCorrectCharacter()
        {
            // Init
            string characterJson = @"{
                '_id': '5bd6dff40708363dcc5929c3',
                'Name': 'Character name',
                'UserId': '5bd6dff40708363dcc5929c4',
                'Gender': 0,
                'Age': 20,
                'Info': 'some text information about character, like history', 
                'RuleSystem': 0,
                'SystemCharacterData': {

                        }
                }";

            JObject jObject = JObject.Parse(characterJson);

            MongoDB.Bson.ObjectId id = new MongoDB.Bson.ObjectId(jObject.Value<string>("_id"));

            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            characterProvider.Setup(c => c.ReplaceCharacter(
                id, It.IsAny<Character>()));

            Mock<IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();

            CharactersController controller = new CharactersController(
                characterProvider.Object,
                jsonArrayBuilder.Object
                );
            
            ContentResult response = (ContentResult)controller.Put(jObject, "someUserId", id.ToString());

            characterProvider.Verify(c => c.ReplaceCharacter(
                id, It.IsAny<Character>()), Times.Once());

            Assert.Equal("", response.Content);
            Assert.Equal(200, response.StatusCode);
        }

        /// <summary>
        /// author - Yury Sokolov
        /// </summary>
        [Fact]
        public void TestPutForInternalError()
        {
            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            characterProvider.Setup(c => c.ReplaceCharacter(
                It.IsAny<MongoDB.Bson.ObjectId>(), It.IsAny<Character>()))
                .Throws(new Exception("test"));

            Mock<IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();

            CharactersController controller = new CharactersController(
                characterProvider.Object,
                jsonArrayBuilder.Object
                );

            string characterJson = @"{
                '_id': '5bd6dff40708363dcc5929c3',
                'Name': 'Character name',
                'UserId': '5bd6dff40708363dcc5929c4',
                'Gender': 0,
                'Age': 20,
                'Info': 'some text information about character, like history', 
                'RuleSystem': 0,
                'SystemCharacterData': {

                        }
                }";

            JObject jObject = JObject.Parse(characterJson);

            ContentResult response = (ContentResult)controller.Put(jObject, "someUserId", "5bd6dff40708363dcc5929c3");

            characterProvider.Verify(c => c.ReplaceCharacter(
                It.IsAny<MongoDB.Bson.ObjectId>(), It.IsAny<Character>()),
                Times.Once());

            Assert.Equal("\"test\"", response.Content);
            Assert.Equal(500, response.StatusCode);
        }

        /// <summary>
        /// author - Yury Sokolov
        /// </summary>
        [Fact]
        public void TestDeleteForCorrectId()
        {
            MongoDB.Bson.ObjectId characterId = new MongoDB.Bson.ObjectId();
            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            characterProvider.Setup(c => c.RemoveCharacter(characterId));

            Mock<IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();

            CharactersController controller = new CharactersController(
                characterProvider.Object,
                jsonArrayBuilder.Object
                );

            ContentResult response = (ContentResult)controller.Delete("userId", characterId.ToString());

            characterProvider.Verify(c => c.RemoveCharacter(characterId), Times.Once);

            Assert.Equal("", response.Content);
            Assert.Equal(200, response.StatusCode);
        }

        /// <summary>
        /// author - Yury Sokolov
        /// </summary>
        [Fact]
        public void TestDeleteForInternalError()
        {
            MongoDB.Bson.ObjectId characterId = new MongoDB.Bson.ObjectId();
            Mock<ICharacterProvider> characterProvider = new Mock<ICharacterProvider>();
            characterProvider.Setup(c => c.RemoveCharacter(characterId))
                .Throws(new Exception("test"));

            Mock<IJsonArrayBuilder> jsonArrayBuilder = new Mock<IJsonArrayBuilder>();

            CharactersController controller = new CharactersController(
                characterProvider.Object,
                jsonArrayBuilder.Object
                );

            ContentResult response = (ContentResult)controller.Delete("userId", characterId.ToString());

            characterProvider.Verify(c => c.RemoveCharacter(characterId), Times.Once);

            Assert.Equal("\"test\"", response.Content);
            Assert.Equal(500, response.StatusCode);
        }
    }
}
