﻿using Moq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Utilits.JsonArray;
using Xunit;

namespace Tests.Core.Utilits
{
    public class JsonArraySeparatorTests
    {
        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestNull()
        {
            //Init
            JsonArraySeparator separator = new JsonArraySeparator();

            string input = null;

            //Action
            var res = separator.SeparateArray(input);

            //Assert
            Assert.Null(res);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestEmptyCollection()
        {
            //Init
            JsonArraySeparator separator = new JsonArraySeparator();

            string input = "[]";

            //Action
            List<JObject> res = (List<JObject>) separator.SeparateArray(input);
            
            //Assert
            Assert.Empty(res);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestNotEmpryCollection()
        {
            //Init
            JsonArraySeparator separator = new JsonArraySeparator();

            string input = "[{\"value1\":1, \"value2\":{\"list\":[{\"v\":1},{\"v\":2}]}},{\"value1\":1, \"value2\":1}]";

            //Action 
            var r = separator.SeparateArray(input);
            List<JObject> res = new List<JObject>(separator.SeparateArray(input));

            //Assert
            Assert.Equal(2, res.Count);
        }
    }
}
