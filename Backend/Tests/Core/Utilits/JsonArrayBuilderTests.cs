﻿using Model.Database;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Utilits.JsonArray;
using Xunit;

namespace Tests.Core.Utilits
{
    public class JsonArrayBuilderTests
    {
        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestNull()
        {
            //Init
            JsonArrayBuilder builder = new JsonArrayBuilder();

            //Action
            var res = builder.BuildArray(null);

            //Assert
            Assert.Null(res);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestEmptyCollection()
        {
            //Init
            JsonArrayBuilder builder = new JsonArrayBuilder();
            List<Character> jsons = new List<Character>();
            jsons.Clear();

            //Action
            var res = builder.BuildArray(jsons);

            //Assert
            Assert.Equal("[]", res);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestNotEmpryCollection()
        {
            //Init
            Mock<IJsonSerializable> mock = new Mock<IJsonSerializable>();
            mock.Setup(j => j.ToJson()).Returns("{}");

            JsonArrayBuilder builder = new JsonArrayBuilder();
            List<IJsonSerializable> jsons = new List<IJsonSerializable>() { mock.Object, mock.Object };

            //Action 
            var res = builder.BuildArray(jsons);

            //Assert
            Assert.Equal("[{},{}]", res);
        }
    }
}
