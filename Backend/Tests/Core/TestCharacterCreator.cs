﻿using Model.Database;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Core
{
    public static class TestCharacterCreator
    {
        public static Character GetTestCharacter()
        {
            Character character = new Character();
            character.Age = TestAge;
            character.Gender = TestGender;
            character.Info = TestInfo;
            character.Name = TestName;
            character.RuleSystem = TestRuleSystem;
            character.SystemCharacterData = TestSystemCharacterData;
            character.UserId = TestUserId;
            character._id = TestId;

            return character;
        }

        public static int TestAge = 18;
        public static Gender TestGender = 0;
        public static string TestInfo = "Info";
        public static string TestName = "Name";
        public static RuleSystem TestRuleSystem = 0;
        public static BsonDocument TestSystemCharacterData = new BsonDocument("field", 0);
        public static string TestUserId = "UserId";
        public static ObjectId TestId = new ObjectId();
    }
}
