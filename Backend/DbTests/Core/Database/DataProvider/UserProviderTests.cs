﻿using Core.Database;
using Core.Database.DataProviders;
using Model.Database;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DbTests.Core.Database.DataProvider
{
    public class UserProviderTests
    {
        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestAddNewUser()
        {
            //Init
            User user = TestUserCreator.GetTestUser();

            string connectionString = "mongodb://localhost:27017";
            string databaseName = "TestDb";
            string collectionName = "Users";
            RpgDbContext context = new RpgDbContext(connectionString, databaseName, "", collectionName);
            UserProvider provider = new UserProvider(context);

            MongoClient client = new MongoClient(connectionString);
            client.DropDatabase(databaseName);
            IMongoDatabase database = client.GetDatabase(databaseName);
            IMongoCollection<User> collection = database.GetCollection<User>(collectionName);

            //Action
            provider.AddNewUser(user);

            //Assert 
            var filter = Builders<User>.Filter.Empty;
            var result = collection.Find(filter).ToList();
            Assert.Single(result);
            User res = result[0];
            Assert.Equal(TestUserCreator.TestEmail, res.Email);
            Assert.Equal(TestUserCreator.TestLogin, res.Login);
            Assert.Equal(TestUserCreator.TestPassword, res.HashedPassword);
            Assert.Equal(TestUserCreator.TestUserRole, res.UserRole);

            //After Test
            client.DropDatabase(databaseName);
        }
    }
}
