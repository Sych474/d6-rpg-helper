﻿using Core.Database;
using Core.Database.DataProviders;
using Model.Database;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DbTests.Core.Database.DataProvider
{
    public class CharacterProviderTests
    {
        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestAddNewCharacterElementCount()
        {
            //Init
            Character character = TestCharacterCreator.GetTestCharacter();

            string connectionString = "mongodb://localhost:27017";
            string databaseName = "TestDb";
            string collectionName = "characters";
            RpgDbContext context = new RpgDbContext(connectionString, databaseName, collectionName, "");
            CharacterProvider provider = new CharacterProvider(context);

            MongoClient client = new MongoClient(connectionString);
            client.DropDatabase(databaseName);
            IMongoDatabase database = client.GetDatabase(databaseName);
            IMongoCollection<Character> collection = database.GetCollection<Character>(collectionName);
            //Action
            provider.AddNewCharacter(character);

            //Assert 
            var filter = Builders<Character>.Filter.Empty;
            var result = collection.Find(filter).ToList();
            Assert.Single(result);
            
            //After Test
            client.DropDatabase(databaseName);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestAddNewCharacterContent()
        {
            //Init
            string connectionString = "mongodb://localhost:27017";
            string databaseName = "TestDb";
            string collectionName = "characters";
            RpgDbContext context = new RpgDbContext(connectionString, databaseName, collectionName, "");
            CharacterProvider provider = new CharacterProvider(context);

            MongoClient client = new MongoClient(connectionString);
            client.DropDatabase(databaseName);
            IMongoDatabase database = client.GetDatabase(databaseName);
            IMongoCollection<Character> collection = database.GetCollection<Character>(collectionName);
            Character character = TestCharacterCreator.GetTestCharacter();

            //Action
            provider.AddNewCharacter(character);

            //Assert 
            var filter = Builders<Character>.Filter.Eq("Name", "Name");
            Character result = collection.Find(filter).First();

            Assert.Equal(TestCharacterCreator.TestAge, result.Age);
            Assert.Equal(TestCharacterCreator.TestGender, result.Gender);
            //Assert.Equal(TestCharacterCreator.TestId, result._id);
            Assert.Equal(TestCharacterCreator.TestInfo, result.Info);
            Assert.Equal(TestCharacterCreator.TestName, result.Name);
            Assert.Equal(TestCharacterCreator.TestRuleSystem, result.RuleSystem);
            Assert.Equal(TestCharacterCreator.TestSystemCharacterData, result.SystemCharacterData);
            Assert.Equal(TestCharacterCreator.TestUserId, result.UserId);

            //After Test
            client.DropDatabase(databaseName);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestReplaceCharacterContent()
        {
            //Init
            string connectionString = "mongodb://localhost:27017";
            string databaseName = "TestDb";
            string collectionName = "characters";
            RpgDbContext context = new RpgDbContext(connectionString, databaseName, collectionName, "");
            CharacterProvider provider = new CharacterProvider(context);

            MongoClient client = new MongoClient(connectionString);
            client.DropDatabase(databaseName);
            IMongoDatabase database = client.GetDatabase(databaseName);
            IMongoCollection<Character> collection = database.GetCollection<Character>(collectionName);

            Character character = TestCharacterCreator.GetTestCharacter();
            provider.AddNewCharacter(character);
            ObjectId currId = character._id;

            var upd = TestCharacterCreator.GetTestCharacter();
            upd.Age = 19;
            upd.Info = "New Info";
            upd.RuleSystem = RuleSystem.DnD4;
            upd.UserId = "New User";
            upd.Gender = Gender.Female;
            upd.Name = "New Name";
            BsonDocument newData = new BsonDocument("new_field", 2);
            upd.SystemCharacterData = newData;

            //Action
            provider.ReplaceCharacter(currId, upd);

            //Assert 
            var filter = Builders<Character>.Filter.Eq("Name", "New Name");
            Character result = collection.Find(filter).First();

            Assert.Equal(19, result.Age);
            Assert.Equal(Gender.Female, result.Gender);
            Assert.Equal(currId, result._id);
            Assert.Equal("New Info", result.Info);
            Assert.Equal("New Name", result.Name);
            Assert.Equal(RuleSystem.DnD4, result.RuleSystem);
            Assert.Equal(newData, result.SystemCharacterData);
            Assert.Equal("New User", result.UserId);

            //After Test
            client.DropDatabase(databaseName);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestRemoveCharacter()
        {
            //Init
            string connectionString = "mongodb://localhost:27017";
            string databaseName = "TestDb";
            string collectionName = "characters";
            RpgDbContext context = new RpgDbContext(connectionString, databaseName, collectionName, "");
            CharacterProvider provider = new CharacterProvider(context);

            MongoClient client = new MongoClient(connectionString);
            client.DropDatabase(databaseName);
            IMongoDatabase database = client.GetDatabase(databaseName);
            IMongoCollection<Character> collection = database.GetCollection<Character>(collectionName);

            Character character = TestCharacterCreator.GetTestCharacter();
            provider.AddNewCharacter(character);
            ObjectId id = character._id;

            //Action
            provider.RemoveCharacter(id);

            //Assert 
            var filter = Builders<Character>.Filter.Empty;
            var result = collection.CountDocuments(filter);

            Assert.Equal(0, result);

            //After Test
            client.DropDatabase(databaseName);
        }

        /// <summary>
        /// author - Sychev Svyatoslav
        /// </summary>
        [Fact]
        public void TestGetCharacters()
        {
            //Init
            string connectionString = "mongodb://localhost:27017";
            string databaseName = "TestDb";
            string collectionName = "characters";
            RpgDbContext context = new RpgDbContext(connectionString, databaseName, collectionName, "");
            CharacterProvider provider = new CharacterProvider(context);

            MongoClient client = new MongoClient(connectionString);
            client.DropDatabase(databaseName);
            IMongoDatabase database = client.GetDatabase(databaseName);
            IMongoCollection<Character> collection = database.GetCollection<Character>(collectionName);

            Character character1 = TestCharacterCreator.GetTestCharacter();
            character1.UserId = "Another userId";
            Character character2 = TestCharacterCreator.GetTestCharacter();
            Character character3 = TestCharacterCreator.GetTestCharacter();

            provider.AddNewCharacter(character1);
            provider.AddNewCharacter(character2);
            provider.AddNewCharacter(character3);

            //Action
            provider.GetCharacters(TestCharacterCreator.TestUserId);

            //Assert 
            var filter = Builders<Character>.Filter.Eq("UserId", TestCharacterCreator.TestUserId);
            var result = collection.CountDocuments(filter);

            Assert.Equal(2, result);

            //After Test
            client.DropDatabase(databaseName);
        }
    }
}
