﻿using Model.Database;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace DbTests.Core
{
    public static class TestUserCreator
    {
        public static User GetTestUser()
        {
            User user = new User();
            user.HashedPassword = TestPassword;
            user.UserRole = TestUserRole;
            user.Email = TestEmail;
            user.Login = TestLogin;
            user._id = TestId;

            return user;
        }

        public static UserRole TestUserRole = 0;
        public static string TestPassword = "Password";
        public static string TestLogin = "login";
        public static string TestEmail = "example@gmail.com";
        public static ObjectId TestId = new ObjectId();
    }
}
