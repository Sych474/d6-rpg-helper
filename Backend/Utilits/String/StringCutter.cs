﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilits.String
{
    public class StringCutter : IStringCutter
    {
        public string CutCharOnEdges(string str, char chr)
        {
            if (str[0] == chr && str[str.Length - 1] == chr)
            {
                return str.Substring(1, str.Length - 2);
            }
            else
            {
                return str;
            }
        }
    }
}
