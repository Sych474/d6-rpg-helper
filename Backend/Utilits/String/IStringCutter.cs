﻿namespace Utilits.String
{
    public interface IStringCutter
    {
        string CutCharOnEdges(string str, char chr);
    }
}