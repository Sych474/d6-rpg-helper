﻿using Model.Database;

namespace Utilits.UserRoleUtilits
{
    public interface IUserRolesToStringConverter
    {
        string ConvertToString(UserRole userRole);
        UserRole ConvertToUserRole(string str);
    }
}