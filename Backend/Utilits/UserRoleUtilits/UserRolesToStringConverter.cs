﻿using Model.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Utilits.UserRoleUtilits
{
    public class UserRolesToStringConverter : IUserRolesToStringConverter
    {
        private const string UserStr = "user";
        private const string AdministratorStr = "admin";

        public string ConvertToString(UserRole userRole)
        {
            switch (userRole)
            {
                case UserRole.User:
                    return UserStr;
                case UserRole.Administrator:
                    return AdministratorStr;
                default:
                    throw new Exception();
            }
        }

        public UserRole ConvertToUserRole(string str)
        {
            if (str.Equals(UserStr)) return UserRole.User;
            if (str.Equals(AdministratorStr)) return UserRole.Administrator;
            throw new Exception();
        }
    }
}
