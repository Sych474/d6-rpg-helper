﻿using Model.Database;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utilits.JsonArray
{
    public class JsonArrayBuilder : IJsonArrayBuilder
    {
        public string BuildArray(IEnumerable<IJsonSerializable> collection)
        {
            JsonToStringFunc<IJsonSerializable> jsonToStringFunc = IJsonSerializableToString;

            return Build(collection, jsonToStringFunc);
        }

        public string BuildArrayFromJObjects(IEnumerable<JObject> collection)
        {
            JsonToStringFunc<JObject> jsonToStringFunc = JObjectToString;

            return Build(collection, jsonToStringFunc);
        }

        public string BuildArrayFromObjects(IEnumerable<object> collection)
        {
            JsonToStringFunc<object> jsonToStringFunc = ObjectToString;

            return Build(collection, jsonToStringFunc);
        }

        delegate string JsonToStringFunc<T>(T obj);

        private string IJsonSerializableToString(IJsonSerializable obj)
        {
            return obj.ToJson();
        }

        private string JObjectToString(JObject obj)
        {
            return obj.ToString(Formatting.None);
        }

        private string ObjectToString(object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.None);
        }

        private string Build<T>(IEnumerable<T> collection, JsonToStringFunc<T> jsonToString)
        {
            if (collection == null) return null;
            string array = "[";
            int i = 0;
            foreach (var item in collection)
            {
                array += jsonToString(item);
                if (i < collection.Count() - 1)
                {
                    array += ',';
                }
                i++;
            }
            array += "]";
            return array;
        }
    }
}
