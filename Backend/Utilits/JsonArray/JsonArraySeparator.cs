﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utilits.JsonArray
{
    public class JsonArraySeparator : IJsonArraySeparator
    {
        public IEnumerable<JObject> SeparateArray(string input)
        {
            if (input == null) return null;
            if (input[0] != '[' && input[input.Length - 1] != ']') return null;

            JArray array = JArray.Parse(input);

            return new List<JObject>(array.Values<JObject>());
        }
    }
}
