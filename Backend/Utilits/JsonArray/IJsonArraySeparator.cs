﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Utilits.JsonArray
{
    public interface IJsonArraySeparator
    {
        IEnumerable<JObject> SeparateArray(string input);
    }
}