﻿using System.Collections.Generic;
using Model.Database;
using Newtonsoft.Json.Linq;

namespace Utilits.JsonArray
{
    public interface IJsonArrayBuilder
    {
        string BuildArray(IEnumerable<IJsonSerializable> collection);
        string BuildArrayFromJObjects(IEnumerable<JObject> collection);
        string BuildArrayFromObjects(IEnumerable<object> collection);
    }
}