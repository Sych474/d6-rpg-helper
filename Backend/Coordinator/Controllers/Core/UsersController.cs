﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coordinator.ApiClients.Core;
using Model.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilits.String;
using Utilits.JsonArray;
using Coordinator.ApiClients.Core.Exceptions;
using Microsoft.AspNetCore.Cors;

namespace Coordinator.Controllers.Core
{
    [EnableCors("AllowAllOrigin")]
    [Route("api/core/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private ICoreUserApiClient _coreUserApiClient;
        private IStringCutter _stringCutter;
        private IJsonArrayBuilder _jsonArrayBuilder;

        public UsersController(ICoreUserApiClient coreUserApiClient, IStringCutter stringCutter, IJsonArrayBuilder jsonArrayBuilder)
        {
            _coreUserApiClient = coreUserApiClient;
            _stringCutter = stringCutter;
            _jsonArrayBuilder = jsonArrayBuilder;
        }

        // GET: api/core/Users/
        [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            int statusCode = 200;
            string response = "";
            try
            {
                var userInfos = await _coreUserApiClient.GetAllUsersInfo();
                response = $"{{\"List\":{_jsonArrayBuilder.BuildArrayFromObjects(userInfos)}}}";
            }
            catch (Exception e)
            {
                statusCode = 500; // Internal server Error
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // POST: api/core/Users/
        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] AddUserInfo addUserInfo)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                if (addUserInfo.UserRole == 1) throw new Exception("Admin user post is unsapported");

                string userId = await _coreUserApiClient.PostUser(addUserInfo);
            }
            catch (UserAlreadyExistsExceprion e)
            {
                statusCode = 409;
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }
            catch (Exception e)
            {
                statusCode = 500; // Internal server Error
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // Put: api/core/Users/
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> PutUser([FromBody] UpdateUserInfo updateUserInfo)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                await _coreUserApiClient.PutUser(updateUserInfo);
            }
            catch (Exception e)
            {
                statusCode = 500; // Internal server Error
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }
    }
}