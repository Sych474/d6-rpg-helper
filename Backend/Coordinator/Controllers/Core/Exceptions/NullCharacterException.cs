﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Coordinator.Controllers.Core.Exceptions
{
    public class NullCharacterException : Exception
    {
        public NullCharacterException()
        {
        }

        public NullCharacterException(string message) : base(message)
        {
        }

        public NullCharacterException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NullCharacterException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
