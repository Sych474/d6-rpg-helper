﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Coordinator.Controllers.Core.Exceptions
{
    public class IncorrectRuleSystemException : Exception
    {
        public IncorrectRuleSystemException()
        {
        }

        public IncorrectRuleSystemException(string message) : base(message)
        {
        }

        public IncorrectRuleSystemException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected IncorrectRuleSystemException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
