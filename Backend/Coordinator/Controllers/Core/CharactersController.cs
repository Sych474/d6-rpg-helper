﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Coordinator.ApiClients;
using Coordinator.ApiClients.Core;
using Coordinator.ApiClients.Core.Exceptions;
using Coordinator.Authorization;
using Coordinator.Authorization.Exceptions;
using Coordinator.Controllers.CommonExceptions;
using Coordinator.Controllers.Core.Exceptions;
using Model.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utilits.JsonArray;
using Utilits.String;
using Microsoft.AspNetCore.Cors;

namespace Coordinator.Controllers.Core
{
    [EnableCors("AllowAllOrigin")]
    [Route("api/core/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private ICoreCharacterApiClient _coreCharacterApiClient;
        private IJsonArrayBuilder _jsonArrayBuilder;
        private IStringCutter _stringCutter;

        public CharactersController(
            ICoreCharacterApiClient coreCharacterApiClient, 
            IJsonArrayBuilder jsonArrayBuilder, 
            IStringCutter stringCutter)
        {
            _coreCharacterApiClient = coreCharacterApiClient;
            _jsonArrayBuilder = jsonArrayBuilder;
            _stringCutter = stringCutter;
        }


        // GET: api/core/Characters?userId=1&ruleSystem=1
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Get(string userId, RuleSystem ruleSystem)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                if (!Enum.IsDefined(typeof(RuleSystem), ruleSystem)) throw new IncorrectRuleSystemException("Unsapported RuleSystem");

                IEnumerable<JObject> characters = await _coreCharacterApiClient.GetCharacters(userId, ruleSystem);
                response = $"{{\"List\":{_jsonArrayBuilder.BuildArrayFromJObjects(characters)}}}";
            }
            catch (UnknownUserIdException e)
            {
                statusCode = 404; // Not found
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }
            catch (IncorrectRuleSystemException e)
            {
                statusCode = 501; // Not Implemented
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }
            catch (HttpRequestException e)
            {
                statusCode = 504; // Gateway Timeout 
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }
            catch (Exception e)
            {
                statusCode = 500; // Internal server Error
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // GET: api/core/Characters/{userId}
        [Authorize]
        [HttpGet(), Route("{userId}")]
        public async Task<IActionResult> Get(string userId)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                IEnumerable<JObject> characters = await _coreCharacterApiClient.GetCharacters(userId);
                response = $"{{\"List\":{_jsonArrayBuilder.BuildArrayFromJObjects(characters)}}}";
            }
            catch (UnknownUserIdException e)
            {
                statusCode = 404; // Not found
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // GET: api/core/Characters/{userId}/{characterId}
        [Authorize]
        [HttpGet(), Route("{userId}/{characterId}")]
        public async Task<IActionResult> GetOne(string userId, string characterId)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                JObject character = await _coreCharacterApiClient.GetCharacter(userId, characterId);

                if (character == null) throw new NullCharacterException();
                response = character.ToString(Formatting.None);
            }
            catch (UnknownUserIdException e)
            {
                statusCode = 404; // Not found
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }
            catch (NullCharacterException e)
            {
                statusCode = 404; // Not found
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }
        // POST: api/core/Characters/{userId}
        [Authorize]
        [HttpPost, Route("{userId}")]
        public async Task<IActionResult> Post([FromBody] JObject character, string userId)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                string characterId = await _coreCharacterApiClient.PostCharacter(character, userId);
                response = $"{{\"CharacterId\":\"{characterId}\"}}";
            }
            catch (UnknownUserIdException e)
            {
                statusCode = 404; // Not found
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // PUT: api/core/Characters/{userId}/{characterId}
        [Authorize]
        [HttpPut("{userId}/{characterId}")]
        public async Task<IActionResult> Put([FromBody] JObject character, string userId, string characterId)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                await _coreCharacterApiClient.PutCharacter(character, userId, characterId);
            }
            catch (UnknownUserIdException e)
            {
                statusCode = 404; // Not found
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // DELETE: api/core/Characters/{userId}/{characterId}
        [Authorize]
        [HttpDelete("{userId}/{characterId}")]
        public async Task<IActionResult> Delete(string userId, string characterId)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                await _coreCharacterApiClient.DeleteCharacter(userId, characterId);
            }
            catch (UnknownUserIdException e)
            {
                statusCode = 404; // Not found
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }
    }
}
