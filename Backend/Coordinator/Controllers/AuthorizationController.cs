﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coordinator.ApiClients.Core;
using Coordinator.Authorization.Jwt;
using Model.Dto;
using Model.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Utilits.String;
using Microsoft.AspNetCore.Cors;

namespace Coordinator.Controllers
{
    [EnableCors("AllowAllOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        private ICoreUserApiClient _coreUserApiClient;
        private IStringCutter _stringCutter;
        private IJwtTockenCreator _jwtTockenCreator;

        public AuthorizationController(ICoreUserApiClient coreUserApiClient, IStringCutter stringCutter, IJwtTockenCreator jwtTockenCreator)
        {
            _coreUserApiClient = coreUserApiClient;
            _stringCutter = stringCutter;
            _jwtTockenCreator = jwtTockenCreator;
        }

        // POST: api/Authorization
        [HttpPost]
        public async Task<IActionResult> Authorize([FromBody] AuthorizationUserInfo authorizationUserInfo)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                UserInfo userInfo = await _coreUserApiClient.GetUserInfo(authorizationUserInfo);
                
                response = $"{{" +
                    $"\"Token\":\"{_jwtTockenCreator.CreateJwt(userInfo)}\"," +
                    $"\"UserInfo\":{JsonConvert.SerializeObject(userInfo)}" +
                    $"}}";
            }
            catch (Exception e)
            {
                statusCode = 500; // Internal server Error
                response = $"\"{_stringCutter.CutCharOnEdges(e.Message, '"')}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }
    }
}