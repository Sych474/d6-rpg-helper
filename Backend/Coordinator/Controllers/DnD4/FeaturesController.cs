﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Coordinator.Controllers.DnD4
{
    [Route("api/dnd4/[controller]")]
    [ApiController]
    public class FeaturesController : ControllerBase
    {
        // GET: api/dnd4/Features
        [HttpGet]
        public IActionResult Get()
        {
            int statusCode = 500;
            string response = "\"Get Features\"";

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }
    }
}
