﻿using Model.Dto;

namespace Coordinator.Authorization.Jwt
{
    public interface IJwtTockenCreator
    {
        string CreateJwt(UserInfo userInfo);
    }
}