﻿using Model.Dto;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Utilits.UserRoleUtilits;
using Model.Database;

namespace Coordinator.Authorization.Jwt
{
    public class JwtTockenCreator : IJwtTockenCreator
    {
        private IUserRolesToStringConverter _userRolesToStringConverter;
        
        public JwtTockenCreator(IUserRolesToStringConverter userRolesToStringConverter)
        {
            _userRolesToStringConverter = userRolesToStringConverter;
        }

        public string CreateJwt(UserInfo userInfo)
        {
            ClaimsIdentity identity = GetIdentity(userInfo);

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                    issuer: AuthentificationOptions.ISSUER,
                    audience: AuthentificationOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthentificationOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthentificationOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private ClaimsIdentity GetIdentity(UserInfo userInfo)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, userInfo.UserId),    
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, UserRoleConverter.ToString(userInfo.UserRole))
                };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}
