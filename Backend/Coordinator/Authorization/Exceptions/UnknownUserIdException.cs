﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Coordinator.Authorization.Exceptions
{
    public class UnknownUserIdException : Exception
    {
        public UnknownUserIdException()
        {
        }

        public UnknownUserIdException(string message) : base(message)
        {
        }

        public UnknownUserIdException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnknownUserIdException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
