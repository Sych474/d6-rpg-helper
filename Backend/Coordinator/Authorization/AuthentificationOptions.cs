﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coordinator.Authorization
{
    public class AuthentificationOptions
    {
        public static string ISSUER { get; set; } = "MyAuthServer"; // издатель токена

        public static string AUDIENCE { get; set; } = "Client"; // потребитель токена

        private static string KEY = "mysupersecret_secretkey!123";   // ключ для шифрации

        public static int LIFETIME = 60; // время жизни токена - 1 минута

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
