﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coordinator.ApiClients.Common
{
    public class ServiceInfo
    {
        public ServiceInfo(string coreServiceUri)
        {
            CoreServiceUri = coreServiceUri;
        }

        public string CoreServiceUri { get; }
    }
}
