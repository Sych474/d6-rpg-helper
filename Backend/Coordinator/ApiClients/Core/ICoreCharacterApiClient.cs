﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Model.Database;
using Newtonsoft.Json.Linq;

namespace Coordinator.ApiClients.Core
{
    public interface ICoreCharacterApiClient
    {
        Task DeleteCharacter(string userId, string characterId);

        Task<JObject> GetCharacter(string userId, string characterId);

        Task<IEnumerable<JObject>> GetCharacters(string userId);

        Task<IEnumerable<JObject>> GetCharacters(string userId, RuleSystem ruleSystem);

        Task<string> PostCharacter(JObject character, string userId);

        Task PutCharacter(JObject character, string userId, string characterId);
    }
}