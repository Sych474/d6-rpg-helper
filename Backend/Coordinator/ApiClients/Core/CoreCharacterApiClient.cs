﻿using Coordinator.ApiClients.Common;
using Coordinator.ApiClients.Core.Exceptions;
using Model.Database;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilits.JsonArray;
using Utilits.String;

namespace Coordinator.ApiClients.Core
{
    public class CoreCharacterApiClient : ICoreCharacterApiClient
    {
        private HttpClient _httpClient;
        private ServiceInfo _serviceInfo;
        private IJsonArraySeparator _jsonArraySeparator;
        private IStringCutter _stringCutter; 

        public CoreCharacterApiClient(HttpClient httpClient, ServiceInfo serviceInfo, IJsonArraySeparator jsonArraySeparator, IStringCutter stringCutter)
        {
            _httpClient = httpClient;
            _serviceInfo = serviceInfo;
            _jsonArraySeparator = jsonArraySeparator;
            _stringCutter = stringCutter;
        }

        public async Task<IEnumerable<JObject>> GetCharacters(string userId)
        {
            IEnumerable<JObject> characters = new List<JObject>();
            try
            {
                string uri = $"http://{_serviceInfo.CoreServiceUri}/api/Characters/{userId}";
                HttpResponseMessage responce = await _httpClient.GetAsync(uri);

                if (responce.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    characters = _jsonArraySeparator.SeparateArray(await responce.Content.ReadAsStringAsync());
                }
                else
                {
                    throw new InternalCoreException(await responce.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return characters;
        }

        public async Task<IEnumerable<JObject>> GetCharacters(string userId, RuleSystem ruleSystem)
        {
            IEnumerable<JObject> characters = new List<JObject>();
            try
            {
                string uri = $"http://{_serviceInfo.CoreServiceUri}/api/Characters?userId={userId}&ruleSystem={(int)ruleSystem}";
                HttpResponseMessage responce = await _httpClient.GetAsync(uri);

                if (responce.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    characters = _jsonArraySeparator.SeparateArray(await responce.Content.ReadAsStringAsync());
                }
                else
                {
                    throw new InternalCoreException(await responce.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return characters;
        }

        public async Task<JObject> GetCharacter(string userId, string characterId)
        {
            JObject character = null;
            try
            {
                string uri = $"http://{_serviceInfo.CoreServiceUri}/api/Characters/{userId}/{characterId}";
                HttpResponseMessage responce = await _httpClient.GetAsync(uri);
                if (responce.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    character = JObject.Parse(await responce.Content.ReadAsStringAsync());
                }
                else
                {
                    throw new InternalCoreException(await responce.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return character;
        }

        public async Task<string> PostCharacter(JObject character, string userId)
        {
            string id = null;
            try
            {
                string uri = $"http://{_serviceInfo.CoreServiceUri}/api/Characters/{userId}";
                HttpResponseMessage responce = await _httpClient.PostAsJsonAsync(uri, character);

                if (responce.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    id = _stringCutter.CutCharOnEdges(await responce.Content.ReadAsStringAsync(), '"');
                }
                else
                {
                    throw new InternalCoreException(await responce.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return id;
        }

        public async Task PutCharacter(JObject character, string userId, string characterId)
        {
            try
            {
                string uri = $"http://{_serviceInfo.CoreServiceUri}/api/Characters/{userId}/{characterId}";
                HttpResponseMessage responce = await _httpClient.PutAsJsonAsync(uri, character);

                if (responce.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new InternalCoreException(await responce.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task DeleteCharacter(string userId, string characterId)
        {
            try
            {
                string uri = $"http://{_serviceInfo.CoreServiceUri}/api/Characters/{userId}/{characterId}";
                HttpResponseMessage responce = await _httpClient.DeleteAsync(uri);

                if (responce.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new InternalCoreException(await responce.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
