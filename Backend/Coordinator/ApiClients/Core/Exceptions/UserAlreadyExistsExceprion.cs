﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Coordinator.ApiClients.Core.Exceptions
{
    public class UserAlreadyExistsExceprion : Exception
    {
        public UserAlreadyExistsExceprion()
        {
        }

        public UserAlreadyExistsExceprion(string message) : base(message)
        {
        }

        public UserAlreadyExistsExceprion(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UserAlreadyExistsExceprion(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
