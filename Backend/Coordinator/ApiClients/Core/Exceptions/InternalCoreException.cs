﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Coordinator.ApiClients.Core.Exceptions
{
    public class InternalCoreException : Exception
    {
        public InternalCoreException()
        {
        }

        public InternalCoreException(string message) : base(message)
        {
        }

        public InternalCoreException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InternalCoreException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
