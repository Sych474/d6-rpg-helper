﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Model.Dto;

namespace Coordinator.ApiClients.Core
{
    public interface ICoreUserApiClient
    {
        Task<IEnumerable<UserInfo>> GetAllUsersInfo();
        Task<UserInfo> GetUserInfo(AuthorizationUserInfo authentificationUserInfo);
        Task<string> PostUser(AddUserInfo addUserInfo);
        Task PutUser(UpdateUserInfo updateUserInfo);
    }
}