﻿using Coordinator.ApiClients.Common;
using Coordinator.ApiClients.Core.Exceptions;
using Model.Dto;
using Model.Database;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilits.JsonArray;

namespace Coordinator.ApiClients.Core
{
    public class CoreUserApiClient : ICoreUserApiClient
    {
        private HttpClient _httpClient;
        private ServiceInfo _serviceInfo;
        private IJsonArraySeparator _jsonArraySeparator;

        public CoreUserApiClient(HttpClient httpClient, ServiceInfo serviceInfo, IJsonArraySeparator jsonArraySeparator)
        {
            _httpClient = httpClient;
            _serviceInfo = serviceInfo;
            _jsonArraySeparator = jsonArraySeparator;
        }

        public async Task<IEnumerable<UserInfo>> GetAllUsersInfo()
        {
            IEnumerable<UserInfo> userInfos = null;
            try
            {
                string uri = $"http://{_serviceInfo.CoreServiceUri}/api/Users";
                HttpResponseMessage responce = await _httpClient.GetAsync(uri);

                if (responce.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var users = _jsonArraySeparator.SeparateArray(await responce.Content.ReadAsStringAsync());
                    userInfos = users.Select(u => JsonConvert.DeserializeObject<UserInfo>(u.ToString())).ToList();
                }
                else
                {
                    throw new InternalCoreException(await responce.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return userInfos;
        }

        public async Task<UserInfo> GetUserInfo(AuthorizationUserInfo authentificationUserInfo)
        {
            UserInfo userInfo = null;
            try
            {
                string uri = $"http://{_serviceInfo.CoreServiceUri}/api/Users/GetUserInfo";
                HttpResponseMessage responce = await _httpClient.PostAsJsonAsync(uri, authentificationUserInfo);

                if (responce.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    userInfo = JsonConvert.DeserializeObject<UserInfo>(await responce.Content.ReadAsStringAsync());
                }
                else
                {
                    throw new InternalCoreException(await responce.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return userInfo;
        }

        public async Task<string> PostUser(AddUserInfo addUserInfo)
        {
            string userId = null;
            try
            {
                string uri = $"http://{_serviceInfo.CoreServiceUri}/api/Users/PostNewUser";
                HttpResponseMessage responce = await _httpClient.PostAsJsonAsync(uri, addUserInfo);

                if (responce.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    userId = await responce.Content.ReadAsStringAsync();
                }
                else if (responce.StatusCode == System.Net.HttpStatusCode.Conflict)
                {
                    throw new UserAlreadyExistsExceprion("User already exists");
                }
                else
                {
                    throw new InternalCoreException(await responce.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return userId;
        }

        public async Task PutUser(UpdateUserInfo updateUserInfo)
        {
            try
            {
                string uri = $"http://{_serviceInfo.CoreServiceUri}/api/Users/PutUser";
                HttpResponseMessage responce = await _httpClient.PutAsJsonAsync(uri, updateUserInfo);

                if (responce.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new InternalCoreException(await responce.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
