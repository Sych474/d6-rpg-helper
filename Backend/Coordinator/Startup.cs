﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Coordinator.ApiClients;
using Coordinator.ApiClients.Common;
using Coordinator.ApiClients.Core;
using Coordinator.Authorization;
using Coordinator.Authorization.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Utilits.JsonArray;
using Utilits.String;
using Utilits.UserRoleUtilits;

namespace Coordinator
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSingleton(new ServiceInfo(Configuration["Core:Uri"]));
            //services.AddSingleton(new AuthentificationOptions());
            services.AddTransient<HttpClient>();
            services.AddTransient<IJsonArrayBuilder, JsonArrayBuilder>();
            services.AddTransient<IUserRolesToStringConverter, UserRolesToStringConverter>();
            services.AddTransient<IJsonArraySeparator, JsonArraySeparator>();
            services.AddTransient<IJwtTockenCreator, JwtTockenCreator>();
            services.AddTransient<IStringCutter, StringCutter>();
            services.AddSingleton<ICoreCharacterApiClient, CoreCharacterApiClient>();
            services.AddSingleton<ICoreUserApiClient, CoreUserApiClient>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            // укзывает, будет ли валидироваться издатель при валидации токена
                            ValidateIssuer = true,
                            // строка, представляющая издателя
                            ValidIssuer = AuthentificationOptions.ISSUER,

                            // будет ли валидироваться потребитель токена
                            ValidateAudience = true,
                            // установка потребителя токена
                            ValidAudience = AuthentificationOptions.AUDIENCE,
                            // будет ли валидироваться время существования
                            ValidateLifetime = true,
                            LifetimeValidator = CustomLifetimeValidator,

                            // установка ключа безопасности
                            IssuerSigningKey = AuthentificationOptions.GetSymmetricSecurityKey(),
                            // валидация ключа безопасности
                            ValidateIssuerSigningKey = true,
                        };
                    });
            services.AddCors(options => options.AddPolicy("AllowAllOrigin", builder => builder
                .WithOrigins("http://localhost:4200")
                .AllowAnyHeader()
                .AllowAnyMethod())
               );
        }

        public static bool CustomLifetimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            if (expires != null)
            {
                return DateTime.UtcNow < expires;
            }
            return false;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("AllowAllOrigin");
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
