﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Core.Controllers.Exceptions
{
    public class WrongAuthentificationUserInfoException : Exception
    {
        public WrongAuthentificationUserInfoException()
        {
        }

        public WrongAuthentificationUserInfoException(string message) : base(message)
        {
        }

        public WrongAuthentificationUserInfoException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected WrongAuthentificationUserInfoException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
