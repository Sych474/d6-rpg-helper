﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Core.Controllers.Exceptions
{
    public class WrongUserInfoException : Exception
    {
        public WrongUserInfoException()
        {
        }

        public WrongUserInfoException(string message) : base(message)
        {
        }

        public WrongUserInfoException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected WrongUserInfoException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
