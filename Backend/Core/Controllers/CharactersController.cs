﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.Database.DataProviders;
using Model.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json.Linq;
using Utilits.JsonArray;

namespace Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterProvider _characterProvider;
        private readonly IJsonArrayBuilder _jsonArrayBuilder;
        public CharactersController(ICharacterProvider characterProvider, IJsonArrayBuilder jsonArrayBuilder)
        {
            _characterProvider = characterProvider;
            _jsonArrayBuilder = jsonArrayBuilder;
        }

        // GET: api/Characters?userId=1&ruleSystem=1
        [HttpGet]
        public IActionResult Get(string userId, int ruleSystem)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                if (userId == null) throw new Exception("Illegal UserId");
                if (!Enum.IsDefined(typeof(RuleSystem), ruleSystem)) throw new Exception("Illegal RuleSystem");

                var characters = _characterProvider.GetCharacters(userId, (RuleSystem) ruleSystem);
                response = _jsonArrayBuilder.BuildArray(characters);
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{e.Message}\"";
            }
            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // GET: api/Characters/1
        [HttpGet("{userId}", Name = "Get")]
        public IActionResult Get(string userId)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                if (userId == null) throw new Exception("Illegal UserId");
                var characters = _characterProvider.GetCharacters(userId);
                response = _jsonArrayBuilder.BuildArray(characters);
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{e.Message}\"";
            }
            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // GET: api/Characters/1/5bd6dff40708363dcc5929c3
        [HttpGet(), Route("{userId}/{characterId}")]
        public IActionResult GetOne(string userId, string characterId)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                var character = _characterProvider.GetCharacter(userId, new ObjectId(characterId));
                response = character.ToJson();
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{e.Message}\"";
            }
            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }


        // POST: api/Characters/{UserId}
        [HttpPost, Route("{userId}")]
        public IActionResult Post([FromBody] JObject json, [FromRoute] string userId)
        {
            //TO_DO Check userId ???
            int statusCode = 200;
            string response = "";
            try
            {
                BsonDocument doc = BsonDocument.Parse(json.ToString());
                Character character = BsonSerializer.Deserialize<Character>(doc);
                
                _characterProvider.AddNewCharacter(character);

                response = $"\"{character._id.ToString()}\"";
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{e.Message}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // PUT: api/Characters/{userId}/{characterId}
        [HttpPut("{userId}/{characterId}")]
        public IActionResult Put([FromBody] JObject json, string userId, string characterId)
        {
            //TO_DO Check userId ???
            int statusCode = 200;
            string response = "";
            try
            {
                BsonDocument doc = BsonDocument.Parse(json.ToString());
                Character character = BsonSerializer.Deserialize<Character>(doc);

                _characterProvider.ReplaceCharacter(new ObjectId(characterId), character);
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{e.Message}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // DELETE: api/Characters/{userId}/{characterId}
        [HttpDelete("{userId}/{characterId}")]
        public IActionResult Delete(string userId, string characterId)
        {
            //TO_DO Check userId ???
            int statusCode = 200;
            string response = "";
            try
            {
                _characterProvider.RemoveCharacter(new ObjectId(characterId));
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{e.Message}\"";
            }

            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }
    }
}
