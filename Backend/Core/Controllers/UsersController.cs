﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Database.DataProviders;
using Model.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Core.Controllers.Exceptions;
using Model.Database;
using Newtonsoft.Json;
using Utilits.JsonArray;
using MongoDB.Bson;
using Core.Database.DataProviders.Exceptions;

namespace Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserProvider _userProvider;
        private IJsonArrayBuilder _jsonArrayBuilder;

        public UsersController(IUserProvider userProvider, IJsonArrayBuilder jsonArrayBuilder)
        {
            _userProvider = userProvider;
            _jsonArrayBuilder = jsonArrayBuilder;
        }


        // POST: api/Users/GetUserInfo
        [HttpPost(), Route("GetUserInfo")]
        public IActionResult GetUserInfo([FromBody] AuthorizationUserInfo authentificationUserInfo)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                if (!ValidateAuthentificationUserInfo(authentificationUserInfo)) throw new WrongAuthentificationUserInfoException();

                User user = _userProvider.GetUser(authentificationUserInfo.Login);

                if (user.HashedPassword != authentificationUserInfo.HashedPassword) throw new WrongAuthentificationUserInfoException();

                UserInfo userInfo = new UserInfo(user);

                response = JsonConvert.SerializeObject(userInfo, Formatting.None);
            }
            catch (WrongAuthentificationUserInfoException e)
            {
                statusCode = 401;
                response = $"\"{e.Message}\"";
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{e.Message}\"";
            }
            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // GET: api/Users
        [HttpGet()]
        public IActionResult GetUsers()
        {
            int statusCode = 200;
            string response = "";
            try
            {
                List<User> users = new List<User>(_userProvider.GetAllUsers());
                List<UserInfo> userInfos = users.Select(u => new UserInfo(u)).ToList();

                response = _jsonArrayBuilder.BuildArrayFromObjects(userInfos);
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{e.Message}\"";
            }
            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // POST: api/Users/PostNewUser
        [HttpPost(), Route("PostNewUser")]
        public IActionResult AddNewUser([FromBody] AddUserInfo addUserInfo)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                if (!ValidateAddUserInfo(addUserInfo)) throw new WrongUserInfoException();

                response = $"\"{_userProvider.AddNewUser(addUserInfo.GetUser())}\"";
            }
            catch (WrongUserInfoException e)
            {
                statusCode = 401;
                response = $"\"{e.Message}\"";
            }
            catch (NonUniqueLoginException e)
            {
                statusCode = 409;
                response = $"\"{e.Message}\"";
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{e.Message}\"";
            }
            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        // PUT: api/Users/PutUser
        [HttpPut(), Route("PutUser")]
        public IActionResult PutUser([FromBody] UpdateUserInfo updateUserInfo)
        {
            int statusCode = 200;
            string response = "";
            try
            {
                if (!ValidateUpdateUserInfo(updateUserInfo)) throw new WrongUserInfoException();
                User oldUser = _userProvider.GetUserById(updateUserInfo.UserId);
                User updUser = updateUserInfo.UpdateUser(oldUser);
                
                _userProvider.ReplaceUser(updUser, updUser.Login);
            }
            catch (WrongUserInfoException e)
            {
                statusCode = 401;
                response = $"\"{e.Message}\"";
            }
            catch (Exception e)
            {
                statusCode = 500;
                response = $"\"{e.Message}\"";
            }
            return new ContentResult() { Content = response, ContentType = "application/json", StatusCode = statusCode };
        }

        private bool ValidateAddUserInfo(AddUserInfo addUserInfo)
        {
            return (
                    addUserInfo != null
                    && addUserInfo.Email != null
                    && addUserInfo.HashedPassword != null
                    && addUserInfo.Login != null
                    && Enum.IsDefined(typeof(UserRole), addUserInfo.UserRole)
                    );
        }

        private bool ValidateUpdateUserInfo(UpdateUserInfo updateUserInfo)
        {
            try
            {
                ObjectId objectId = new ObjectId(updateUserInfo.UserId); 
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private bool ValidateAuthentificationUserInfo(AuthorizationUserInfo authentificationUserInfo)
        {
            return (
                    authentificationUserInfo != null
                    && authentificationUserInfo.Login != null
                    && authentificationUserInfo.Login != ""
                    && authentificationUserInfo.HashedPassword != null
                    && authentificationUserInfo.HashedPassword != ""
                    );
        }
    }
}