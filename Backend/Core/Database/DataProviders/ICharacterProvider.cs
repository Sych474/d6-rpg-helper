﻿using System.Collections.Generic;
using Model.Database;
using MongoDB.Bson;

namespace Core.Database.DataProviders
{
    public interface ICharacterProvider
    {
        ObjectId AddNewCharacter(Character character);

        Character GetCharacter(string userId, ObjectId id);

        IEnumerable<Character> GetCharacters(string userId);

        IEnumerable<Character> GetCharacters(string userId, RuleSystem ruleSystem);

        void RemoveCharacter(ObjectId objectId);

        void ReplaceCharacter(ObjectId objectId, Character character);
    }
}