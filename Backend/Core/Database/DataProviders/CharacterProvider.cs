﻿using Model.Database;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Database.DataProviders
{
    public class CharacterProvider : BaseDataProvider, ICharacterProvider
    {
        public CharacterProvider(RpgDbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Character> GetCharacters(string userId)
        {
            IEnumerable<Character> characters = null;
            try
            {
                var filter = Builders<Character>.Filter.Eq("UserId", userId);
                characters = dbContext.Characters.Find(filter).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return characters;
        }

        public IEnumerable<Character> GetCharacters(string userId, RuleSystem ruleSystem)
        {
            IEnumerable<Character> characters = null;
            try
            {
                var builder = new FilterDefinitionBuilder<Character>();
                var filter = builder.Eq("UserId", userId) & builder.Eq("RuleSystem", (int) ruleSystem);
                characters = dbContext.Characters.Find(filter).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return characters;
        }

        public Character GetCharacter(string userId, ObjectId id)
        {
            Character character = null;
            try
            {
                var builder = new FilterDefinitionBuilder<Character>();
                var filter = builder.Eq("_id", id) & builder.Eq("UserId", userId);
                character = dbContext.Characters.Find(filter).First();
            }
            catch (Exception)
            {
                throw;
            }
            return character;
        }

        public ObjectId AddNewCharacter(Character character)
        {
            try
            {
                dbContext.Characters.InsertOne(character);
            }
            catch (Exception)
            {
                throw;
            }
            return character._id;
        }

        public void ReplaceCharacter(ObjectId objectId, Character character)
        {
            try
            {
                character._id = objectId;
                BsonDocument filter = new BsonDocument("_id", objectId);
                dbContext.Characters.ReplaceOne(filter, character);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void RemoveCharacter(ObjectId objectId)
        {
            try
            {
                BsonDocument filter = new BsonDocument("_id", objectId);
                dbContext.Characters.DeleteOne(filter);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
