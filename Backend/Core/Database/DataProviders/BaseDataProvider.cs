﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Database.DataProviders
{
    public class BaseDataProvider
    {
        protected RpgDbContext dbContext;

        public BaseDataProvider(RpgDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
    }
}
