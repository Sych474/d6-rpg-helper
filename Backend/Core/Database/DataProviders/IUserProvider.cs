﻿using System.Collections.Generic;
using Model.Database;
using MongoDB.Bson;

namespace Core.Database.DataProviders
{
    public interface IUserProvider
    {
        ObjectId AddNewUser(User user);
        ICollection<User> GetAllUsers();
        User GetUser(string login);
        User GetUserById(string userId);
        void ReplaceUser(User user, string login);
    }
}