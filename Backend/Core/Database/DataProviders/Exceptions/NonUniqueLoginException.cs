﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Core.Database.DataProviders.Exceptions
{
    public class NonUniqueLoginException : Exception
    {
        public NonUniqueLoginException()
        {
        }

        public NonUniqueLoginException(string message) : base(message)
        {
        }

        public NonUniqueLoginException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NonUniqueLoginException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
