﻿using Core.Database.DataProviders.Exceptions;
using Model.Database;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Database.DataProviders
{
    public class UserProvider : BaseDataProvider, IUserProvider
    {
        public UserProvider(RpgDbContext dbContext) : base(dbContext)
        {
        }

        public User GetUser(string login)
        {
            User user = null;
            try
            {
                var builder = new FilterDefinitionBuilder<User>();
                var filter = builder.Eq("Login", login);
                user = dbContext.Users.Find(filter).First();
            }
            catch (Exception)
            {
                throw;
            }
            return user;
        }

        public User GetUserById(string userId)
        {
            User user = null;
            try
            {
                var builder = new FilterDefinitionBuilder<User>();
                var filter = builder.Eq("_id", new ObjectId(userId));
                user = dbContext.Users.Find(filter).First();
            }
            catch (Exception)
            {
                throw;
            }
            return user;
        }

        public ICollection<User> GetAllUsers()
        {

            ICollection<User> users = null;
            try
            {
                var builder = new FilterDefinitionBuilder<User>();
                var filter = builder.Empty;
                users = dbContext.Users.Find(filter).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return users;
        }

        public ObjectId AddNewUser(User user)
        {
            try
            {
                var builder = new FilterDefinitionBuilder<User>();
                var filter = builder.Eq("Login", user.Login);
                if (dbContext.Users.CountDocuments(filter) != 0)
                {
                    throw new NonUniqueLoginException("Login already exists");
                }
                else
                {
                    dbContext.Users.InsertOne(user);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return user._id;
        }

        public void ReplaceUser(User user, string login)
        {
            try
            {
                BsonDocument filter = new BsonDocument("Login", login);
                dbContext.Users.ReplaceOne(filter, user);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
