﻿using Model.Database;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Database
{
    public class RpgDbContext : BaseMongoDbContext
    {
        private string charactersCollectionName;
        private string userCollectionname;

        public RpgDbContext(string connectionString, string databaseName, string charactersCollectionName, string userCollectionname) : base(connectionString, databaseName)
        {
            this.charactersCollectionName = charactersCollectionName;
            this.userCollectionname = userCollectionname;
        }

        public IMongoCollection<Character> Characters
        {
            get { return database.GetCollection<Character>(charactersCollectionName); }
        }

        public IMongoCollection<User> Users
        {
            get { return database.GetCollection<User>(userCollectionname); }
        }
    }
}
