var express = require('express');
var app = express();

  //http://localhost:9000/api/Characters/1 - correct
  //http://localhost:9000/api/Characters/2 - correct empty
  //http://localhost:9000/api/Characters/3 - error
app.get('/api/Characters/:userId', function (req, res) {
    if (req.params.userId == 1)
    {
        res.json([
            {
                _id: new String("CharacterId1"),
                Name: new String("Name1"),
                UserId: new String("UserId1"),
                Gender: new Number(0),
                Age: new Number(20),
                Info: new String("Info"), 
                RuleSystem: new Number(1),
                SystemCharacterData: {SomeData: new String("some data")}
            },
            {
                _id: new String("CharacterId2"),
                Name: new String("Name2"),
                UserId: new String("UserId2"),
                Gender: new Number(0),
                Age: new Number(20),
                Info: new String("Info"), 
                RuleSystem: new Number(1),
                SystemCharacterData: {SomeData: new String("some data")}
            }
        ]);
    }
    else if (req.params.userId == 2) 
    {
        res.json([])
    }
    else
    {
        res.statusCode = 500;
        res.json("Error");
    }
});

  //http://localhost:9000/api/Characters?userId=1&ruleSystem=1 - correct
  //http://localhost:9000/api/Characters?userId=2&ruleSystem=1 - correct empty
  //http://localhost:9000/api/Characters?userId=3&ruleSystem=1 - error
app.get('/api/Characters', function (req, res) {
    console.log('%s %s', req.query.userId, req.query.ruleSystem);
    const user = req.query.userId
    const ruleSystem = req.query.ruleSystem
    if (user == 1 && ruleSystem == 1)
    {
        res.json([
            {
                _id: new String("CharacterId1"),
                Name: new String("Name1"),
                UserId: new String("UserId1"),
                Gender: new Number(0),
                Age: new Number(20),
                Info: new String("Info"), 
                RuleSystem: new Number(1),
                SystemCharacterData: {SomeData: new String("some data")}
            },
            {
                _id: new String("CharacterId2"),
                Name: new String("Name2"),
                UserId: new String("UserId2"),
                Gender: new Number(0),
                Age: new Number(20),
                Info: new String("Info"), 
                RuleSystem: new Number(1),
                SystemCharacterData: {SomeData: new String("some data")}
            }
        ]);
    }
    else if (user == 2) 
    {
        res.json([])
    }
    else
    {
        res.statusCode = 500;
        res.json("Error");
    }
  });

  //http://localhost:9000/api/Characters/1/1 - correct
  //http://localhost:9000/api/Characters/2/1 - error
  app.get('/api/Characters/:userId/:characterId', function (req, res) {
      if(req.params.userId == 1)
      {
        res.json({
            _id: new String("CharacterId1"),
            Name: new String("Name1"),
            UserId: new String("UserId1"),
            Gender: new Number(0),
            Age: new Number(20),
            Info: new String("Info"), 
            RuleSystem: new Number(1),
            SystemCharacterData: {SomeData: new String("some data")}
        });
      }
      else 
      {
        res.statusCode = 500;
        res.json("Error");
      }
  });

  //http://localhost:9000/api/Users/GetUserInfo
  /* correct body
  {
      "Login":"user",
      "HashedPassword":"pass"
  }
  */
  app.post('/api/Users/GetUserInfo', function (req, res) {
    //Console.log(req.body);
    //if (req.body["Login"] == "user" && req.body["Password"] == "pass")
    if(true)
    {
      res.json({
          Login: new String("user"),
          UserId: new String("000"),
          Email: new String("email@mail.ru"),
          UserRole: new Number(0)
      });
    }
    else 
    {
      res.statusCode = 500;
      res.json("Error");
    }
});

var server = app.listen(9000, function () {
    var host = server.address().address
    var port = server.address().port
    console.log('Server app listening at http://%s:%s', host, port)
});
