﻿using Model.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Dto
{
    public class UpdateUserInfo
    {
        public string UserId { get; set; }

        public string NewHashedPassword { get; set; }

        public UserRole? NewUserRole { get; set; }

        public string NewEmail { get; set; }

        public User UpdateUser(User user)
        {
            User res = new User();

            if (UserId != user._id.ToString()) throw new Exception();
            res._id = user._id;
            res.HashedPassword = this.NewHashedPassword != null ? this.NewHashedPassword : user.HashedPassword;
            res.UserRole = this.NewUserRole.HasValue ? this.NewUserRole.Value : user.UserRole;
            res.Email = this.NewEmail != null ? this.NewEmail : user.Email;
            res.Login = user.Login;

            return res;
        }
    }
}
