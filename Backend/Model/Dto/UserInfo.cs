﻿using Model.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Dto
{
    public class UserInfo
    {
        public UserInfo()
        {
        }

        public UserInfo(User user)
        {
            this.Login = user.Login;
            this.UserId = user._id.ToString();
            this.Email = user.Email;
            this.UserRole = user.UserRole;
        }
        

        public string Login { get; set; }

        public string UserId { get; set; }

        public string Email { get; set; }

        public UserRole UserRole { get; set; }
    }
}
