﻿using Model.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Dto
{
    public class AddUserInfo
    {
        public string Login { get; set; }

        public string HashedPassword { get; set; }
        
        public string Email { get; set; }

        public int UserRole { get; set; }

        public User GetUser()
        {
            User user = new User();
            user.Login = this.Login;
            user.HashedPassword = this.HashedPassword;
            user.UserRole = (UserRole) this.UserRole;
            user.Email = this.Email;
            user._id = new MongoDB.Bson.ObjectId();

            return user;
        }
    }
}
