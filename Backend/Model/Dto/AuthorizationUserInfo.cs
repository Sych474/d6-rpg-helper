﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Dto
{
    public class AuthorizationUserInfo
    {
        public string Login { get; set; }

        public string HashedPassword { get; set; }
    }
}
