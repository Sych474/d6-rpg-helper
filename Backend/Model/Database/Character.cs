﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Database
{
    public class Character : IJsonSerializable
    {
        [BsonId]
        public ObjectId _id { get; set; }

        public string Name { get; set; }

        public string UserId { get; set; }

        public Gender Gender { get; set; }

        public int Age { get; set; }

        public string Info { get; set; }

        public RuleSystem RuleSystem { get; set; }

        public BsonDocument SystemCharacterData { get; set; }

        public string ToJson()
        {
            return "{\"_id\":" + '"' + _id + '"' +
                ",\"Name\":" + '"' + Name + '"' +
                ",\"UserId\":" + '"' + UserId + '"' +
                ",\"Gender\":" + (int)Gender +
                ",\"Age\":" + Age +
                ",\"Info\":" + '"' + Info + '"' +
                ",\"RuleSystem\":" + (int)RuleSystem +
                ",\"SystemCharacterData\":" + SystemCharacterData + "}";
        }
    }
}
