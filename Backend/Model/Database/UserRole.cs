﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Database
{
    public enum UserRole
    {
        User = 0, 
        Administrator = 1,
    }

    public static class UserRoleConverter
    {
        public static string ToString(UserRole userRole)
        {
            switch (userRole)
            {
                case UserRole.User:
                    return "user";
                case UserRole.Administrator:
                    return "admin";
                default:
                    //TO_DO special exception
                    throw new Exception();
            }
        }
    }
}
