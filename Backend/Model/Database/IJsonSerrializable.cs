﻿namespace Model.Database
{
    public interface IJsonSerializable
    {
        string ToJson();
    }
}