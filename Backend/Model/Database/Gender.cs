﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Database
{
    public enum Gender
    {
        Unknown = 0,
        Male = 1,
        Female = 2,
    }
}
