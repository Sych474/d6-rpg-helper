﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Database
{
    public class User
    {
        public ObjectId _id { get; set; }

        public string Login { get; set; }

        public string HashedPassword { get; set; }

        public string Email { get; set; }

        public UserRole UserRole { get; set; }
    }
}
